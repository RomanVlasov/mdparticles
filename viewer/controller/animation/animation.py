from __future__ import print_function
import weakref
import states.user_control
import states.play

__author__ = 'Roman Vlasov'


class AnimationController(object):
    """
    Class controls playback of animation
    """
    def __init__(self):
        self._visual_particles = None
        self._animation_data = None
        self._current_frame_index = 0
        self._previous_frame_index = -1
        self._state = None
        self._observers = weakref.WeakSet()
        self.pause()

    @property
    def visual_particles(self):
        return self._visual_particles

    @visual_particles.setter
    def visual_particles(self, value):
        self._visual_particles = value

    @property
    def animation_data(self):
        return self._animation_data

    @animation_data.setter
    def animation_data(self, value):
        self._animation_data = value

    @property
    def current_frame_index(self):
        return self._current_frame_index

    @current_frame_index.setter
    def current_frame_index(self, value):
        self._current_frame_index = value

    def change_state(self, new_state):
        """
        Change inner state of animation controller.
        Called from inner states of controller
        """
        self._state = new_state
        self._state.controller = self

    def pause(self):
        """
        Pause animation playback
        """
        self.change_state(states.user_control.UserControlState())

    def play_switch(self):
        """
        Play/pause switch event
        """
        self._state.on_play_switch()

    def switch_to_last_frame(self):
        """
        Show last frame and stop
        """
        self.pause()
        self.current_frame_index = self._animation_data.get_last_frame_index()

    def switch_to_first_frame(self):
        """
        Show first frame and stop
        """
        self.pause()
        self.current_frame_index = 0

    def is_playing(self):
        """
        Returns true if animation is currently
        playing
        """
        return isinstance(self._state, states.play.PlayState)

    def force_set_frame(self, frame_index):
        """
        Forcefully set frame ignoring animation playback state
        """
        self.pause()
        self.current_frame_index = frame_index

    def get_frame_index_bounds(self):
        """
        Returns tuple: min nad max frame index
        """
        return 0, self._animation_data.get_last_frame_index()

    @property
    def current_system_state(self):
        """
        Get current particle system state
        """
        return self._animation_data.get_state(self._current_frame_index)

    def mark_particle(self, index, mark):
        """
        Mark/unmark particle visually with specific color
        """
        self._visual_particles.mark_particle(index, mark)

    def update(self, delta_seconds):
        """
        Update with delta seconds
        """
        if self._animation_data is None:
            return
        elif self._visual_particles is None:
            return

        self._state.update(delta_seconds)
        state = self._animation_data.get_state(self._current_frame_index)
        self._visual_particles.update_positions(state.particles)

        if self._current_frame_index == self._animation_data.get_last_frame_index():
            self.pause()

        if self._current_frame_index != self._previous_frame_index:
            for observer in self._observers:
                observer.update_animation(self)
            self._previous_frame_index = self._current_frame_index

    def add_observer(self, observer):
        """
        Observer will receive notifications of frame index changes
        """
        observer.update_animation(self)
        self._observers.add(observer)