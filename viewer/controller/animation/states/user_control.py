import base
import play

__author__ = 'Roman Vlasov'


class UserControlState(base.State):
    def __init__(self):
        base.State.__init__(self)

    def on_play_switch(self):
        self.controller.change_state(play.PlayState())

    def update(self, delta_seconds):
        ##################################
        # Just do nothing because
        # user controls animation manually
        ##################################
        pass