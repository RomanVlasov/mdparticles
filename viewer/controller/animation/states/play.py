import base
import user_control

__author__ = 'Roman Vlasov'


class PlayState(base.State):
    time_adjust_coef = 0.1
    def __init__(self):
        base.State.__init__(self)
        self._time = None

    def on_play_switch(self):
        self.controller.change_state(user_control.UserControlState())

    def update(self, delta_seconds):
        if self._time is None:
            self._init_time()

        self._time += self.time_adjust_coef * delta_seconds
        new_index = self.controller.animation_data.get_frame_index(self._time)
        self.controller.current_frame_index = new_index

    def _init_time(self):
        index = self.controller.current_frame_index
        self._time = self.controller.animation_data.get_frame_time(index)

