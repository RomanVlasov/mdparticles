__author__ = 'Roman Vlasov'


class State(object):
    """
    Base state of animtaion controller.
    Used simple state programming pattern.
    """
    def __init__(self):
        self._controller = None

    @property
    def controller(self):
        return self._controller

    @controller.setter
    def controller(self, value):
        self._controller = value

    def on_play_switch(self):
        """
        Play/pause switch event
        """
        pass

    def update(self, delta_seconds):
        """
        Update with delta seconds
        """
        pass