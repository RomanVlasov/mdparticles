import app as main


if __name__ == "__main__":
    app = main.App()
    app.start_main_loop()