import matplotlib
matplotlib.use('WXAgg')

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure

import wx

__author__ = 'Roman Vlasov'


class CanvasPanel(wx.Panel):
    """
    Canvas for matplotlib figure
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.figure = Figure()
        self._axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(self.sizer)
        self.Fit()

    @property
    def axes(self):
        return self._axes

    def autoscale(self):
        self._axes.relim()
        self._axes.autoscale_view()
        self.canvas.draw()



if __name__ == "__main__":
    app = wx.PySimpleApp()
    fr = wx.Frame(None, title='test')
    panel = CanvasPanel(fr)
    panel.draw()
    fr.Show()
    app.MainLoop()


