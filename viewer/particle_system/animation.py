from __future__ import print_function
import system_state
import system
import collections
import bisect
from itertools import islice

__author__ = 'Roman Vlasov'


class Animation(object):
    """
    Main class for particle system evolution. Consists of initial particle system
    and records of particle states connected to specific time period.
    """
    def __init__(self):
        self._system = system.ParticleSystem()
        self._system_states = collections.OrderedDict()

    def __repr__(self):
        repr_list = list()
        for key in self._system_states.keys():
            repr_list.append(("time:{}".format(key), self._system_states[key]))

        return "\n".join(str(i) for i in repr_list)

    def init_with_json(self, json_dict):
        """
        Initialization from json dictionary with
        all necessary configuration data.
        """
        animation_data = json_dict["animation"]
        if animation_data is not None:
            #First of all initialize system settings
            self._system.init_with_json(animation_data["particle_system"])
            #Only after it show particle states
            system_states = animation_data["particle_states"]
            if isinstance(system_states, list):
                for state in system_states:
                    if isinstance(state, list):
                        time = state[1]
                        assert(isinstance(time, float))
                        new_system_state = system_state.SystemState(self._system)
                        new_system_state.init_with_json(state[0])
                        self._system_states[time] = new_system_state

    def init_with_config_file(self, filepath):
        """
        Initialization from configuration file
        """
        import json
        with open(filepath, "r") as config:
            json = json.loads(config.read())
            config.close()
            self.init_with_json(json)

    @property
    def time_bounds(self):
        """
        Return time bounds of the animation
        """
        if len(self._system_states.keys()) == 0:
            return None
        else:
            return self._system_states.keys()[0], self._system_states.keys()[-1]

    @property
    def all_states(self):
        return self._system_states

    def get_frame_time(self, index):
        """
        Returns time in seconds of animation frame
        by frame index
        """
        return self._system_states.keys()[index]

    def get_frame_index(self, time):
        """
        Returns frame index by time in seconds
        """
        index = bisect.bisect_left(self._system_states.keys(), time)
        if index >= len(self._system_states.keys()):
            index = len(self._system_states.keys()) - 1
        return index

    def get_last_frame_index(self):
        return len(self._system_states.keys()) - 1

    def get_state(self, index):
        """
        Get system state at index
        """
        key = self._system_states.keys()[index]
        return self._system_states[key]

    def get_all_states_up_to(self, index):
        """
        Get dictionary of system states before index
        """
        return collections.OrderedDict(islice(self._system_states.iteritems(), index))


if __name__ == "__main__":
    animation = Animation()
    animation.init_with_config_file("../examples/ten_particle_system.json")
    print("{}".format(animation))

