import numpy
import particle

__author__ = 'Roman Vlasov'


class SystemState():
    """
    State of particle system
    """
    def __init__(self, system):
        self._particles = list()
        self._kinetic_energy = None
        self._potential_energy = None
        self._system = system

    def __repr__(self):
        return str(self._particles)

    def init_with_json(self, json_list):
        """
        Initialize system state with configuration file
        and particle system with main settings
        """
        for data in json_list:
            new_particle = particle.Particle()
            new_particle.init_with_json(data)
            self._particles.append(new_particle)

        self._cache_energies()

    @property
    def particles(self):
        """
        Returns list of particles
        """
        return self._particles

    @property
    def kinetic_energy(self):
        return self._kinetic_energy

    @property
    def potential_energy(self):
        return self._potential_energy

    def _cache_energies(self):
        """
        Cache energy value for each system state
        """
        self._kinetic_energy = numpy.mean(list(i.kinetic_energy for i in self._particles))
        self._potential_energy = 0.0
        for i in range(len(self._particles)):
            for j in range(i+1, len(self._particles)):
                particle1 = self._particles[i]
                particle2 = self._particles[j]
                self._potential_energy += self._system.get_potential(i, j).calculate_energy(particle1, particle2)
