import numpy as np

__author__ = 'Roman Vlasov'

class Particle():
    """
    Class represents particle
    and its parameters
    """
    def __init__(self):
        self._position = None
        self._velocity = None

    def __repr__(self):
        return "pos:{} vel:{}".format(self._position, self._velocity)

    def init_with_json(self, json_dict):
        """
        Initialize particle with configuration dictionary
        """
        self._position = np.array(json_dict["pos"])
        self._velocity = np.array(json_dict["vel"])

    @property
    def position(self):
        return self._position

    @property
    def velocity(self):
        return self._velocity

    @property
    def kinetic_energy(self):
        velocity_norm = np.linalg.norm(self._velocity)
        return (velocity_norm ** 2) / 2.0


