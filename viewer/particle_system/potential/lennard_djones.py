import base
import numpy as np

__author__ = 'Roman Vlasov'


class LennardDjonesPotential(base.PairPotential):
    def __init__(self, abs_min_value, min_value_distance):
        """
        :param abs_min_value: minimum value of Lennard-Djones potential
        :param min_value_distance: distance corresponding to minimum value
        """
        base.PairPotential.__init__(self)
        self._min_value_distance_pow6 = min_value_distance ** 6
        self._min_value_distance = min_value_distance
        self._abs_min_value = abs_min_value

    def calculate_energy(self, particle1, particle2):
        distance = particle2.position - particle1.position
        return self.calculate_energy_by_distance(distance)

    def calculate_energy_by_distance(self, distance):
        """
        Lennard-Djones potential depends only on distance between
        particles. No information about mass needed.
        """
        distance_squared = np.inner(distance, distance)
        tmpcoef = self._min_value_distance_pow6 / (distance_squared ** 3)
        return self._abs_min_value * tmpcoef * (tmpcoef - 2.0)