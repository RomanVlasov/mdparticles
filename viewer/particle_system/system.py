import potential.lennard_djones

__author__ = 'Roman Vlasov'


class ParticleSystem():
    """
    Class represents particle system we are working with.
    It is loaded from configuration file generated by
    MDParticles calculation module.

    ATTENTION: It is not working properly
    and used hardcoded LennardDjones potential now
    """

    def __init__(self):
        self._potential = None

    def init_with_json(self, json_dict):
        """
        Initialize system with json configuration file
        """
        self._potential = potential.lennard_djones.LennardDjonesPotential(10.0, 0.75)

    def get_potential(self, particle_index1, particle_index2):
        """
        Return potential for specified particles
        :param particle_index1: index of first particle
        :param particle_index2: index of second particle
        ATTENTION: For now it will just return hardcoded potential.
        """
        return self._potential


