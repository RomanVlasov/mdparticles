import forms.forms as forms
import wx

__author__ = 'Roman Vlasov'


class AnimationControlPanel(forms.AnimationControlPanel):
    """
    Class represents media controlling panel
    redirects events to abstract animation controller
    """

    def __init__(self, parent):
        self._controller = None
        forms.AnimationControlPanel.__init__(self, parent)

        self._timer = wx.Timer(self)
        self._timer.Start(50)
        self.Bind(wx.EVT_TIMER, self._onupdate, self._timer)
        self.anim_control_slider.Bind(wx.EVT_SLIDER, self._on_animslide)

        self._onupdate(None)

    @property
    def controller(self):
        return self._controller

    @controller.setter
    def controller(self, value):
        self._controller = value

    def on_last_frame(self, event):
        if self._controller is None:
            return
        self._controller.switch_to_last_frame()

    def on_first_frame(self, event):
        if self._controller is None:
            return
        self._controller.switch_to_first_frame()

    def on_play_switch(self, event):
        """
        Handler of tool selection events
        """
        if self._controller is None:
            return

        self._controller.play_switch()
        self._onupdate(None)

    def _add_play_control(self, control_id, image):
        self.anim_control_toolbar.InsertLabelTool(1, control_id, u"switch", wx.Bitmap(image, wx.BITMAP_TYPE_ANY),
                                                  wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None)
        self.Bind(wx.EVT_TOOL, self.on_play_switch, id=control_id)

    # noinspection PyUnusedLocal
    def _onupdate(self, event):
        if (self._controller is not None) and self._controller.is_playing():
            self.anim_control_toolbar.EnableTool(forms.wx.ID_PLAY, False)
            self.anim_control_toolbar.EnableTool(forms.wx.ID_PAUSE, True)
        else:
            self.anim_control_toolbar.EnableTool(forms.wx.ID_PAUSE, False)
            self.anim_control_toolbar.EnableTool(forms.wx.ID_PLAY, True)

        self.anim_control_toolbar.Realize()
        if self._controller:
            bounds = self._controller.get_frame_index_bounds()
            self.anim_control_slider.SetMin(bounds[0])
            self.anim_control_slider.SetMax(bounds[1])
            self.anim_control_slider.SetValue(self._controller.current_frame_index)

    def _on_animslide(self, event):
        if self._controller is not None:
            self._controller.force_set_frame(self.anim_control_slider.GetValue())

