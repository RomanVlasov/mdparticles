import wx

import forms.forms as forms
import helper_graph
import potential_graph

__author__ = 'Roman Vlasov'


class SettingsPanel(forms.SettingsPanel):
    """
    Settings panel with points list
    and visual settings tab
    """
    def __init__(self, parent):
        forms.SettingsPanel.__init__(self, parent)
        self._controller = None

        self._timer = wx.Timer(self)
        self._timer.Start(50)
        self.Bind(wx.EVT_TIMER, self._onupdate, self._timer)

        self.particle_position_list.InsertColumn(0, "N", width=20)
        self.particle_position_list.InsertColumn(1, "x", width=100)
        self.particle_position_list.InsertColumn(2, "y", width=100)
        self.particle_position_list.InsertColumn(3, "z", width=100)

    @property
    def controller(self):
        return self._controller

    @controller.setter
    def controller(self, value):
        self._controller = value

    def _onupdate(self, event):
        if self._controller is not None:
            particles = self._controller.current_system_state.particles
            for i in range(len(particles)):
                particle = particles[i]

                particle_list = self.particle_position_list
                if particle_list.GetItemCount() <= i:
                    particle_list.Append((i, 0, 0, 0))

                particle_list.SetStringItem(i, 1, str(particle.position[0]))
                particle_list.SetStringItem(i, 2, str(particle.position[1]))
                particle_list.SetStringItem(i, 3, str(particle.position[2]))

    def on_select_item(self, event):
        selected_index = event.GetIndex()
        if self._controller is not None:
            self._controller.mark_particle(selected_index, True)

    def on_deselect_item(self, event):
        selected_index = event.GetIndex()
        if self._controller is not None:
            self._controller.mark_particle(selected_index, False)

    def on_add_helper_graph(self, event):
        #######################################
        # Show helper graph
        #######################################
        graph = helper_graph.HelperGraph(self)
        graph.Show(True)
        self._controller.add_observer(graph)

    def on_show_potential_graph(self, event):
        #######################################
        # Show potential graph
        #######################################
        graph = potential_graph.PotentialGraph(self)
        graph.Show(True)
        self._controller.add_observer(graph)


