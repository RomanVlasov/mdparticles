import wx
import forms.forms as forms
import plot2d.canvas

__author__ = 'Roman Vlasov'


class HelperGraph(forms.HelperGraph):
    """
    Helper dialog to display graph
    """
    def __init__(self, parent):
        forms.HelperGraph.__init__(self, parent)
        self._controller = None
        self._bind_to_controller = True
        self._canvas = plot2d.canvas.CanvasPanel(self)
        self.plotpanel_sizer.Add(self._canvas, 1, wx.EXPAND | wx.ALL, 5)
        self._lines = list()
        self._lines.append(self._canvas.axes.plot([], [], 'g-', label="Kinetic")[0])
        self._lines.append(self._canvas.axes.plot([], [], 'b-', label="Potential")[0])
        self._lines.append(self._canvas.axes.plot([], [], 'r-', label="Total")[0])

    @property
    def controller(self):
        return self._controller

    @controller.setter
    def controller(self, value):
        self._controller = value
        self.update_animation(self._controller)

    def update_animation(self, controller):
        """
        Update animation from controller
        """
        data = controller.animation_data
        states = data.get_all_states_up_to(controller.current_frame_index) if self._bind_to_controller else data.all_states
        
        points = list(value.kinetic_energy for key, value in states.iteritems())
        self._lines[0].set_xdata(states.keys())
        self._lines[0].set_ydata(points)

        points2 = list(value.potential_energy for key, value in states.iteritems())
        self._lines[1].set_xdata(states.keys())
        self._lines[1].set_ydata(points2)

        points3 = list(value.potential_energy + value.kinetic_energy for key, value in states.iteritems())
        self._lines[2].set_xdata(states.keys())
        self._lines[2].set_ydata(points3)

        self._canvas.autoscale()
