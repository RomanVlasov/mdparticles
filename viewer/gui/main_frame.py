import forms.forms as forms
import plot3d.canvas as canvas

class MainFrame(forms.MainFrame):
    """
    Main widget. Controls all children widgets, catches events and etc.
    """
    def __init__(self, parent):
        """
        :param parent: wxWidgets parent control
        """
        forms.MainFrame.__init__(self, parent)
        canvas.create_canvas(self)