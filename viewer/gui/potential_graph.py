import wx
import forms.forms as forms
import plot2d.canvas
import numpy as np
import particle_system.potential.lennard_djones

__author__ = 'Roman Vlasov'


class PotentialGraph(forms.HelperGraph):
    """
    Helper dialog to display graph
    """
    def __init__(self, parent):
        forms.HelperGraph.__init__(self, parent)
        self._controller = None
        self._bind_to_controller = True
        self._canvas = plot2d.canvas.CanvasPanel(self)
        self.plotpanel_sizer.Add(self._canvas, 1, wx.EXPAND | wx.ALL, 5)
        self._lines = list()
        self._lines.append(self._canvas.axes.plot([], [], 'g-')[0])

        self._canvas.axes.axhline(color='blue')
        self._canvas.axes.axvline(color='blue')


        self.show_graph()

    def show_graph(self):
        potential = particle_system.potential.lennard_djones.LennardDjonesPotential(10.0, 0.75)
        xdata = np.linspace(0.6, 3.0, num=500)
        ydata = list(potential.calculate_energy_by_distance(distance) for distance in xdata)
        self._lines[0].set_xdata(xdata)
        self._lines[0].set_ydata(ydata)
        self._canvas.autoscale()

    def update_animation(self, controller):
        pass
