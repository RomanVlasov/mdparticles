from __future__ import division, print_function

import wx
from visual import rate, arrow

import plot3d.canvas
import plot3d.particles
import gui.settings_panel as settings_panel
import gui.animation_control as animation_control
import particle_system.animation as anim
from controller.animation.animation import AnimationController

__author__ = 'Roman Vlasov'


class App():
    def __init__(self):
        self._animation = None
        self._display = None
        self._visual_particles = plot3d.particles.Particles()
        self._animation_controller = AnimationController()

        self._create_windows()
        self._create_coordinate_system()
        self._load_animation()
        self._configure_animation_controller()

    def _create_windows(self):
        #######################################
        # Creating wxWidgets application
        # We are using forms generated with wxFormBuilder
        #######################################
        anim_canvas_size = (740, 640)
        frame, panel, self._display = plot3d.canvas.create_canvas(anim_canvas_size)

        #######################################
        # Initialize main frame sizer
        #######################################
        main_sizer = wx.BoxSizer(wx.HORIZONTAL)

        #######################################
        # Configuring animation control panel
        #######################################
        animation_control_panel = animation_control.AnimationControlPanel(frame)
        animation_control_panel.controller = self._animation_controller
        animation_control_sizer = wx.BoxSizer(wx.VERTICAL)
        animation_control_sizer.Add(panel, 2, wx.EXPAND | wx.ALL, 5)
        animation_control_sizer.Add(animation_control_panel, 0, wx.EXPAND | wx.ALL, 5)

        #######################################
        # Add animation control sizer to animation sizer
        #######################################
        main_sizer.Add(animation_control_sizer, 2.5, wx.EXPAND | wx.ALL, 5)

        #######################################
        # Settings panel
        #######################################
        right_panel = settings_panel.SettingsPanel(frame)
        right_panel.controller = self._animation_controller
        main_sizer.Add(right_panel, 1, wx.EXPAND | wx.ALL, 5)

        #######################################
        # Finally update frame
        #######################################
        frame.SetSizerAndFit(main_sizer)
        frame.Centre(wx.BOTH)
        frame.Layout()

    @staticmethod
    def _create_coordinate_system():
        arrow(pos=(0, 0, 0), axis=(0, 0, 1), shaftwidth=1)
        arrow(pos=(0, 0, 0), axis=(0, 1, 0), shaftwidth=1)
        arrow(pos=(0, 0, 0), axis=(1, 0, 0), shaftwidth=1)

    def _load_animation(self):
        config_file = "examples/ten_particle_system.json"
        self._animation = anim.Animation()
        self._animation.init_with_config_file(config_file)
        print("Loaded system {}:".format(config_file))
        print("{}".format(self._animation))

    def _configure_animation_controller(self):
        self._animation_controller.animation_data = self._animation
        self._animation_controller.visual_particles = self._visual_particles

    def _update(self, delta_seconds):
        """
        Update in seconds
        """
        self._animation_controller.update(delta_seconds)

    def start_main_loop(self):
        """
        Start main application loop
        """
        const_rate = 60
        time_step = 1.0/const_rate

        while True:
            rate(const_rate)
            self._update(time_step)


