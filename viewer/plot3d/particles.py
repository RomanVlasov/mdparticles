from visual import sphere, color

__author__ = 'Roman Vlasov'


class Particles:
    """
    Class represents visual representation of particles
    in display
    """
    def __init__(self):
        self._points = list()

    def update_positions(self, particles):
        """
        Update position based on received particles array
        """
        if len(self._points) == 0:
            for particle in particles:
                self._points.append(sphere(make_trail=False))

        for i in range(len(particles)):
            self._points[i].pos = particles[i].position

    def mark_particle(self, index, mark):
        """
        Mark/unmark particle visually with specific color
        """
        particle = self._points[index]
        ####################################################
        # Very interesting feature - trajectory of particle
        # could be saved
        ####################################################
        #particle.make_trail = mark
        particle.color = color.red if mark else color.white



