from __future__ import division, print_function
from visual import *
import wx

__author__ = 'Roman Vlasov'


def create_canvas(display_size):
    """
    Function returns display VPython object, wx.Frame
    and main wx.Panel to work with
    """
    dsize = wx.Size(display_size[0], display_size[1])

    w = window(width=(dsize.x + window.dwidth), height=dsize.y + window.dheight + window.menuheight,  menus=True, title='Widgets')

    d = 0 #20
    animation_control_offset = 150
    display_height = dsize.y - 2*d - animation_control_offset
    display_obj = display(window=w, x=d, y=20, width=dsize.x - 2*d, height=display_height, forward=-vector(0, 1, 2))

    return w.win, w.panel, display_obj