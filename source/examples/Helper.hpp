#pragma once

#include <boost/shared_ptr.hpp>

namespace SolidSystem {

typedef boost::shared_ptr<class Particle> ParticlePtr;

namespace examples
{
	ParticlePtr PARTICLE( double x, double y, double z );
}
}