#include "TwoParticlesExample.hpp"

#include <iostream>
#include <fstream>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/mpi/communicator.hpp>
#include <cpplog/cpplog.hpp>

#include <source/Calculation/Methods/VerletIII.hpp>
#include <source/Calculation/Calculator.hpp>
#include <source/Calculation/Decomposition/Dispensers/RowInterleaving.hpp>
#include <source/Calculation/Synchronizer/Synchronizer.hpp>

#include <source/App/App.hpp>
#include <source/PotentialSystem/Potentials/LennardDjones.hpp>
#include <source/Animation/MDAnimation.hpp>

namespace SolidSystem {
namespace examples
{

	void TwoParticlesExample:: runSimulation()
	{
		using namespace calculation;
		Calculator calculator;

		////////////////////////////////////////////////////////////////////////
		// ����������� ������� ������������ �����
		////////////////////////////////////////////////////////////////////////
		ParticleSystemPtr system( new ParticleSystem() );

		math::Vector newPos1( math::ZERO_VECTOR );
		ParticlePtr particle1( new Particle( newPos1, 1.0f ) );
		system->addParticle( particle1 );

		math::Vector newPos2( math::ZERO_VECTOR );
		newPos2[2] = 1;
		ParticlePtr particle2( new Particle( newPos2, 1.0f ) );
		system->addParticle( particle2 );
		system->setPotential( particle1, particle2, MDPairPotentialPtr( new LennardDjones( 10.0f, 0.75f ) ) );

		calculator.setSystem( system );

		////////////////////////////////////////////////////////////////////////
		// ���������� ��������� ����� ����� � ����� III
		////////////////////////////////////////////////////////////////////////
		calculator.setMethod( SimulationMethodPtr( new VerletIIIMethod() ) );

		////////////////////////////////////////////////////////////////////////
		// ���������� �������������� ������������ ����� �� �����������.
		// �������� ������������� - force-row interleaving.
		// TODO: ������ �� ����������.
		// TODO: �� ��������� ������������ � ������������
		////////////////////////////////////////////////////////////////////////
		boost::mpi::communicator communicator;
		Dispenser::Config config;
		config.currentProcessor = communicator.rank();
		config.totalProcesses = communicator.size();
		calculator.setDispenser( DispenserPtr( new RowInterleavingDispenser( config ) ) );

		////////////////////////////////////////////////////////////////////////
		// ���������� �������������
		// TODO: �� ������� ������ �������� ���� ���������� ��� boost::mpi
		// ������������� ������ ���� �����������
		////////////////////////////////////////////////////////////////////////
		calculator.setSynchronizer( SynchronizerPtr( new Synchronizer() ) );

		////////////////////////////////////////////////////////////////////////
		// ���������� ��������� �������
		////////////////////////////////////////////////////////////////////////
		MDParams params;
		params.startTime = 0.0f;
		params.endTime = 1.0f;
		params.timeDelta = 0.01f;
		calculator.setCalculationParams( params );

		////////////////////////////////////////////////////////////////////////
		// ������� ��������� ������
		////////////////////////////////////////////////////////////////////////
		MDAnimationPtr animation = calculator.run();

		////////////////////////////////////////////////////////////////////////
		// ������� ��������� ������
		////////////////////////////////////////////////////////////////////////
		if( animation )
		{
			saveData( *animation );
		}
		else
		{
			LOG_WARN( App::logger() ) << "Calculator couldn't calculate result. Check log messages above.";
		}
	}

	void TwoParticlesExample::saveData( const MDAnimation& animation )
	{
		std::cout << "Saving animation data" << std::endl;

		std::ofstream saveFile;
		saveFile.open( "two_particles_example_animation.xml" );
		boost::archive::xml_oarchive xml( saveFile );

		xml.template register_type<MDPairPotential>();
		xml.template register_type<LennardDjones>();
		xml << boost::serialization::make_nvp( "animation", animation );
		saveFile.close();
	}

	void TwoParticlesExample::doRun()
	{
		runSimulation();
	}

}
}