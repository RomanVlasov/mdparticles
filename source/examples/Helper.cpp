#include "Helper.hpp"
#include <source/ParticleSystem/Particle.hpp>

namespace SolidSystem {
namespace examples
{
	ParticlePtr PARTICLE( double x, double y, double z )
	{
		math::Vector newPos( math::ZERO_VECTOR );
		newPos[0] = x;
		newPos[1] = y;
		newPos[2] = z;
		const double mass = 1.0;

		ParticlePtr particle( new Particle( newPos, mass ) );
		return particle;
	}
}
}