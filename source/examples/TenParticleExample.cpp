#include "TenParticlesExample.hpp"

#include <iostream>
#include <fstream>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/mpi/communicator.hpp>
#include <cpplog/cpplog.hpp>
#include <json_spirit/json_spirit_writer.h>

#include <source/Calculation/Methods/VerletIII.hpp>
#include <source/Calculation/Calculator.hpp>
#include <source/Calculation/Decomposition/Dispensers/RowInterleaving.hpp>
#include <source/Calculation/Synchronizer/Synchronizer.hpp>

#include <source/App/App.hpp>
#include <source/Serialization/JsonArchive.hpp>
#include <source/PotentialSystem/Potentials/LennardDjones.hpp>
#include <source/Animation/MDAnimation.hpp>

#include "Helper.hpp"

namespace SolidSystem {
namespace examples
{
	void TenParticlesExample:: runSimulation()
	{
		using namespace calculation;
		Calculator calculator;

		////////////////////////////////////////////////////////////////////////
		// ����������� ������� ������������ �����
		////////////////////////////////////////////////////////////////////////
		ParticleSystemPtr system( new ParticleSystem() );
		system->addParticle( PARTICLE( 0, 0, 0 ) );
		system->addParticle( PARTICLE( 1, 0, 0 ) );
		system->addParticle( PARTICLE( 0, 1, 0 ) );
		system->addParticle( PARTICLE( 0, 0, 1 ) );
		system->addParticle( PARTICLE( 1, 0, 1 ) );
		system->addParticle( PARTICLE( 0, 1, 1 ) );
		system->addParticle( PARTICLE( 1, 1, 0 ) );
		system->addParticle( PARTICLE( 1, 1, 1 ) );
		system->addParticle( PARTICLE( 2, 2, 2 ) );
		system->addParticle( PARTICLE( 2, 1, 0 ) );

		////////////////////////////////////////////////////////////////////////
		// ����������� 
		////////////////////////////////////////////////////////////////////////
		system->setPotentialForAll( MDPairPotentialPtr( new LennardDjones( 10.0f, 0.75f ) ) );

		calculator.setSystem( system );

		////////////////////////////////////////////////////////////////////////
		// ���������� ��������� ����� ����� � ����� III
		////////////////////////////////////////////////////////////////////////
		calculator.setMethod( SimulationMethodPtr( new VerletIIIMethod() ) );

		////////////////////////////////////////////////////////////////////////
		// ���������� �������������� ������������ ����� �� �����������.
		// �������� ������������� - force-row interleaving.
		// TODO: ������ �� ����������.
		// TODO: �� ��������� ������������ � ������������
		////////////////////////////////////////////////////////////////////////
		boost::mpi::communicator communicator;
		Dispenser::Config config;
		config.currentProcessor = communicator.rank();
		config.totalProcesses = communicator.size();
		calculator.setDispenser( DispenserPtr( new RowInterleavingDispenser( config ) ) );

		////////////////////////////////////////////////////////////////////////
		// ���������� �������������
		// TODO: �� ������� ������ �������� ���� ���������� ��� boost::mpi
		// ������������� ������ ���� �����������
		////////////////////////////////////////////////////////////////////////
		calculator.setSynchronizer( SynchronizerPtr( new Synchronizer() ) );

		////////////////////////////////////////////////////////////////////////
		// ���������� ��������� �������
		////////////////////////////////////////////////////////////////////////
		MDParams params;
		params.startTime = 0.0f;
		params.endTime = 1.0f;
		params.timeDelta = 0.01f;
		calculator.setCalculationParams( params );

		////////////////////////////////////////////////////////////////////////
		// ������� ��������� ������
		////////////////////////////////////////////////////////////////////////
		MDAnimationPtr animation = calculator.run();

		////////////////////////////////////////////////////////////////////////
		// ������� ��������� ������
		////////////////////////////////////////////////////////////////////////
		if( animation )
		{
			saveData( *animation );
		}
		else
		{
			LOG_WARN( App::logger() ) << "Calculator couldn't calculate result. Check log messages above.";
		}
	}

	void TenParticlesExample::saveData( const MDAnimation& animation )
	{
		std::cout << "Saving animation data" << std::endl;

		std::ofstream saveFile;
		saveFile.open( "two_particles_example_animation.xml" );
		boost::archive::xml_oarchive xml( saveFile );

		json_spirit::Object object;
		serialization::JsonOutputArchive json( object, 1 );

		//xml.template register_type<MDPairPotential>();
		//xml.template register_type<LennardDjones>();
		//xml << boost::serialization::make_nvp( "animation", animation );

		//json.template register_type<MDPairPotential>();
		//json.template register_type<LennardDjones>();
		json << boost::serialization::make_nvp( "animation", animation );

		std::ofstream os( "ten_particle_system.json" );

		json_spirit::write( object, os, json_spirit::pretty_print );

		saveFile.close();
	}

	void TenParticlesExample::doRun()
	{
		runSimulation();
	}

}
}