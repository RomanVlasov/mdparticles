#pragma once

#include <boost/shared_ptr.hpp>

namespace SolidSystem {
namespace examples
{
	/**
	������� ����������� ���������
	��� ��������� ��������
	*/
	class ExampleBase
	{
	public:
		virtual ~ExampleBase() {}

	public:
		void run();

	private:
		virtual void doRun() = 0;
	};

	typedef boost::shared_ptr<ExampleBase> ExampleBasePtr;
}
}