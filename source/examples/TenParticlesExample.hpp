#pragma once

#include "ExampleBase.hpp"

namespace SolidSystem {

class MDAnimation;

namespace examples
{
	/**
	������ ��������� ������� �� ���� �����,
	����������������� � ����������� ��������-������
	*/
	class TenParticlesExample : public ExampleBase
	{
	public:
		void runSimulation();
	
	private:
		void saveData( const MDAnimation& animation );

	private:
		virtual void doRun();

	};
}

}