#include "Particle.hpp"
#include <boost/numeric/ublas/io.hpp>

namespace SolidSystem
{

#pragma mark Constructors

	Particle::Particle()
	: params_( 0 )
	{
	}

	Particle::Particle( const math::Vector& position_in, MassType mass_in )
	: params_( mass_in ) 
	{
		state_.position = position_in;
		state_.velocity = math::ZERO_VECTOR;
	}

#pragma mark Utility functions

	void Particle::getState( State& state ) const
	{
		state = state_;
	}

	void Particle::print( std::ostream& stream ) const
	{
		stream << "Position: " << state_.position << std::endl;
		stream << "Velocity: "<< state_.velocity << std::endl;
		stream << "Mass: " << params_.mass << std::endl;
	}

	std::ostream& operator<< ( std::ostream& stream, const SolidSystem::Particle& particle )
	{
		particle.print( stream );
		return stream;
	}
}