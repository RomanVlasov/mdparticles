#pragma once

#include <boost/shared_ptr.hpp>

#include <source/SystemDefines.hpp>

namespace SolidSystem
{


	/*
	Class represents a particle in molecular dynamics meaning
	*/
	class Particle
	{
	public:
		typedef double MassType;

	public:
		struct Params
		{
			Params( MassType mass_in ) : mass( mass_in ) {}

			MassType mass; 
		};

		struct State
		{
			math::Vector position;
			math::Vector velocity;
			math::Vector acceleration;

			template<class Archive>
			void serialize( Archive & ar, const unsigned int version )
			{
				using boost::serialization::make_nvp;
				ar & make_nvp( "pos", position );
				ar & make_nvp( "vel", velocity );
			}
		};

	public:
		Particle();
		Particle( const math::Vector& position, MassType mass );

	public:
		void getState( State& state ) const;
		MassType getMass() const { return params_.mass; }

		void print( std::ostream& stream ) const;

	public:
		//C�����������
		template<class Archive>
		void serialize( Archive & ar, const unsigned int version )
		{
			using boost::serialization::make_nvp;
			ar & make_nvp( "mass", params_.mass );
			ar & make_nvp( "state", state_ );
		}

	private:
		State state_;
		Params params_; 
	};

	typedef boost::shared_ptr<Particle> ParticlePtr;
	typedef boost::shared_ptr<const Particle> ParticleConstPtr;
	typedef Particle::State ParticleState;
	typedef boost::shared_ptr<Particle::State> ParticleStatePtr;
	typedef boost::shared_ptr<const Particle::State> ParticleStateConstPtr;

	std::ostream& operator<< ( std::ostream& stream, const SolidSystem::Particle& particle );
}
