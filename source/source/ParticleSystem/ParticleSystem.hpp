#pragma once

#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include <source/PotentialSystem/MDPotential.hpp>
#include "Particle.hpp"

namespace SolidSystem
{
	typedef std::vector<ParticlePtr> Particles;

	/*
	�����, ����������� ����� ������������� ������� �������
	*/
	class ParticleSystem
	{
	public:
		ParticleSystem();
		~ParticleSystem();

		//������ � ������������ ������ �������
		const Particles& particles() const;

	public:
		class State;

	public:
		//������ � ������������� �������
		void addParticle( ParticlePtr particle );

		//��������� ��������� ��� ���� ������������ �����
		void setPotential( ParticleConstPtr particle1, ParticleConstPtr particle2, MDPairPotentialPtr potential );
        void setPotential( int particleIndex1, int particleIndex2, MDPairPotentialPtr potential );
		void setPotentialForAll( MDPairPotentialPtr potential );

		void addParticles( const Particles& particles, MDPairPotentialPtr potential );

		//��������� ���� ����������� �� �������� ������������ ����� � �������� @particleIndex2
		//�� ������������ ����� � �������� @particleIndex1
		math::Vector calculateForce( int particleIndex1, int particleIndex, const math::Vector& distance ) const;

	public:
		template<class Archive>
		void serialize( Archive& ar, const unsigned int version );

	private:
		struct Implementation;
		typedef boost::shared_ptr<Implementation> ImplPtr;
		ImplPtr pImpl;
	};

	typedef boost::shared_ptr<ParticleSystem> ParticleSystemPtr;
	typedef boost::weak_ptr<ParticleSystem> ParticleSystemWeakPtr;
	typedef boost::shared_ptr<const ParticleSystem> ParticleSystemConstPtr;
	typedef boost::weak_ptr<const ParticleSystem> ParticleSystemConstWeakPtr;
}
