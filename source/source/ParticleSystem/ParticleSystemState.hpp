#pragma once

#include "ParticleSystem.hpp"
#include <source/SystemDefines.hpp>
#include <boost/multi_array.hpp>

namespace SolidSystem
{
	/*
	�����, ����������� ��������� ������� ������������ �����.
	*/
	class ParticleSystem::State
	{
	public:
		typedef int ParticleIndex;
		typedef std::pair<ParticleIndex, ParticleIndex> ParticlePair;
		typedef std::vector<ParticlePair> ParticlePairs;

	public:
		/*
		��� ������������� ��������� "������" ��������� 
		�� ����� � ������ ��������� �������
		*/
		State( ParticleSystemConstWeakPtr system );
		~State() {}

	public:
		/*
		������ ���� ����������� �� ����� @particleIndex �� ������� ��������� �������
		*/
		math::Vector getForce( ParticleIndex particleIndex ) const;
		math::Vector getAcceleration( ParticleIndex particleIndex ) const;

		/*
		��������� ���� � ��������� � �������
		�������� @particleIndexes �������� �� ��, �������������� ����� ������ ������� ����� ���������
		*/
		void calculateForces( const ParticlePairs& particleIndexes );
		void calculateAllForces();

		//���������� ���� �������������� ����� ����� ������� �������
		math::Vector getInterconnectionForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2 ) const;
		void setInterconnectionForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2, const math::Vector& force );

	public:
		//������ � ���������� ������������ ����� �������
		ParticleState& getParticleState( int particleIndex );
		void getAllStates( std::vector<ParticleState>& states ) const;

	public:
		//������ ��������� �������
		void print( std::ostream& stream ) const;

	private:
		//��������������� �������
		ParticleConstPtr getParticle( ParticleIndex particleIndex ) const;

		//����������� ���� �������������� ����� ����� ������� �������
		math::Vector calculateInterconnectionForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2 ) const;

		void initForces();
		void calculateAndCacheForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2 );
		
	private:
		std::vector<ParticleState> particleStates_;
		boost::multi_array<math::Vector, 2> forces_;
		ParticleSystemConstWeakPtr system_;
	};

	//����� ��������� ������� �� ������
	std::ostream& operator<< ( std::ostream& stream, const SolidSystem::ParticleSystem::State& particle );
}


