#include "ParticleSystem.hpp"

#include <vector>
#include <map>

#include <boost/multi_array.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/array.hpp>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

#include <source/PotentialSystem/MDPotential.hpp>
#include <source/Serialization/JsonArchive.hpp>
#include "Particle.hpp"


namespace SolidSystem
{
	namespace
	{
		const int UNDEFINED_INDEX = -1;
	}

	struct ParticleSystem::Implementation
	{
		typedef boost::multi_array<MDPairPotentialPtr, 2> PotentialTable;
		typedef std::vector<ParticlePtr> ParticleArray;

		PotentialTable potentials;
		Particles particles;

	public:
		bool containes( ParticleConstPtr particle ) const
		{
			return std::find( particles.begin(), particles.end(), particle ) != particles.end();
		}

		int indexOf( ParticleConstPtr particle ) const
		{
			ParticleArray::const_iterator iter = std::find( particles.begin(), particles.end(), particle );
			if( iter != particles.end() )
			{
				return iter - particles.begin();
			}
			return UNDEFINED_INDEX;
		}

	public:
		/*
		 ����������/��������
		 */
		BOOST_SERIALIZATION_SPLIT_MEMBER()

		/*
		����������� ���������� ������� ��� 
		������ JsonOutputArchive
		*/
		void serialize( serialization::JsonOutputArchive& archive,  const unsigned int version )
		{
			save( archive, version );
		}

		template<class Archive>
		void save( Archive & ar, const unsigned int version ) const
		{
			using boost::serialization::make_nvp;
			using boost::serialization::make_array;

			PotentialTable::size_type n0 = potentials.shape()[0];
			ar << BOOST_SERIALIZATION_NVP( n0 );
			PotentialTable::size_type n1 = potentials.shape()[1];
			ar << BOOST_SERIALIZATION_NVP( n1 );

			//ar << make_nvp( "potentials", std::vector<MDPairPotentialPtr>( potentials.data(), potentials.data() + potentials.num_elements() ) );
				//make_array( potentials.data(), potentials.num_elements() );
			ar << make_nvp( "particles", particles );
		}

		template<class Archive>
		void load( Archive & ar, const unsigned int version )
		{
			using boost::serialization::make_nvp;
			using boost::serialization::make_array;
		
			PotentialTable::size_type n0;
			ar >> BOOST_SERIALIZATION_NVP( n0 );
			PotentialTable::size_type n1;
			ar >> BOOST_SERIALIZATION_NVP( n1 );

			potentials.resize( boost::extents[n0][n1] );
			ar >> make_array( potentials.data(), potentials.num_elements() );
			ar >> make_nvp( "particles", particles );
		}
	};


	////////////////////////////////////////////
	// Constructors/desctructors
	////////////////////////////////////////////
	ParticleSystem::ParticleSystem()
	: pImpl( new Implementation() )
	{
	}

	ParticleSystem::~ParticleSystem()
	{
	}

	const Particles& ParticleSystem::particles() const
	{
		return pImpl->particles;
	}

	template<class Archive>
	void ParticleSystem::serialize( Archive& ar, const unsigned int version )
	{
		using boost::serialization::make_nvp;
		ar & make_nvp( "impl", pImpl );
	}

	template void ParticleSystem::serialize<boost::archive::xml_oarchive>( 
		boost::archive::xml_oarchive& ar, 
		const unsigned int version 
	);

	template void ParticleSystem::serialize<serialization::JsonOutputArchive>( 
		serialization::JsonOutputArchive& ar, 
		const unsigned int version 
	);

	////////////////////////////////////////////
	// Adding particles
	////////////////////////////////////////////
	void ParticleSystem::addParticle( ParticlePtr particle )
	{
		pImpl->particles.push_back( particle );

		const int size = pImpl->particles.size();
		pImpl->potentials.resize( boost::extents[size][size] );
	}

	void ParticleSystem::setPotential( ParticleConstPtr particle1, ParticleConstPtr particle2, MDPairPotentialPtr potential )
	{
		if( !pImpl->containes( particle1 ) )
		{
			return;
		}

		if( !pImpl->containes( particle2 ) )
		{
			return;
		}

		const int particleIndex1 = pImpl->indexOf( particle1 );
		const int particleIndex2 = pImpl->indexOf( particle2 );
		
		setPotential( particleIndex1, particleIndex2, potential );
	}

	void ParticleSystem::setPotential( int particleIndex1, int particleIndex2, MDPairPotentialPtr potential )
	{
		BOOST_ASSERT( particleIndex1 < pImpl->particles.size() );
		BOOST_ASSERT( particleIndex2 < pImpl->particles.size() );

		pImpl->potentials[particleIndex1][particleIndex2] = potential;
		pImpl->potentials[particleIndex2][particleIndex1] = potential;
	}

	void ParticleSystem::setPotentialForAll( MDPairPotentialPtr potential )
	{
		for( int i = 0; i < pImpl->potentials.shape()[0]; i++ )
		{
			for( int j = 0; j < pImpl->potentials.shape()[1]; j++ )
			{
				setPotential( i, j, potential );;
			}
		}
	}

	math::Vector ParticleSystem::calculateForce( int particleIndex1, int particleIndex2, const math::Vector& distance ) const
	{
		ParticleConstPtr particle1 = pImpl->particles[particleIndex1];
		ParticleConstPtr particle2 = pImpl->particles[particleIndex2];
		return pImpl->potentials[particleIndex1][particleIndex2]->calculateGradU( particle1, particle2, distance );
	}
}
