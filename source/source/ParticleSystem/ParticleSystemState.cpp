#include "ParticleSystemState.hpp"

#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace SolidSystem
{
	namespace
	{
		/*
		��������������� �������, ����������� ��������� ������������ ����� � ��������� ������ �������
		*/
		ParticleState getInitialState( ParticleConstPtr particle )
		{
			if( particle )
			{
				Particle::State state;
				particle->getState( state );

				return ParticleState( state );
			}
			return ParticleState();
		}
	}

#pragma mark Constructor

	ParticleSystem::State::State( ParticleSystemConstWeakPtr system )
	: system_( system )
	{
		ParticleSystemConstPtr pSystem = system_.lock();
		if( pSystem )
		{
			std::transform( pSystem->particles().begin(), 
							pSystem->particles().end(), 
							std::back_inserter( particleStates_ ), 
							&SolidSystem::getInitialState );
		}

		initForces();
	}

#pragma mark Working with forces

	void ParticleSystem::State::initForces()
	{
		ParticleSystemConstPtr pSystem = system_.lock();
		if( !pSystem )
		{
			return;
		}
	
		const int tableSize = pSystem->particles().size();
		forces_.resize( boost::extents[tableSize][tableSize] );
		std::fill( forces_.origin(), forces_.origin() + forces_.num_elements(), math::ZERO_VECTOR );
	}

	void ParticleSystem::State::calculateAndCacheForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2 )
	{
		if( particleIndex1 == particleIndex2 )
		{
			return;
		}

		const math::Vector force = calculateInterconnectionForce( particleIndex1, particleIndex2 );
		forces_[particleIndex1][particleIndex2] = force;
		forces_[particleIndex2][particleIndex1] = -force;
	}

	void ParticleSystem::State::calculateForces( const ParticlePairs& particleIndexes )
	{
		typedef std::pair<ParticleIndex, ParticleIndex> ParticlePair;
		BOOST_FOREACH( const ParticlePair& pair, particleIndexes )
		{
			calculateAndCacheForce( pair.first, pair.second );
		}
	}

	void ParticleSystem::State::calculateAllForces()
	{
		const size_t n0 = forces_.shape()[0];
		const size_t n1 = forces_.shape()[1];
		for( size_t i = 0; i < forces_.shape()[0]; i++ )
		{
			//��� ������������� ��������� ��� ������� ���, ��� ��� ��� ������ ���� ������������
			for( size_t j = 0; j < i; j++ )
			{
				calculateAndCacheForce( i, j );
			}
		}
	}

#pragma mark Calculating functions

	math::Vector ParticleSystem::State::getForce( int particleIndex ) const
	{
		BOOST_ASSERT( particleIndex >= 0 );
		BOOST_ASSERT( particleIndex < particleStates_.size() );

		math::Vector forceResult = math::ZERO_VECTOR;

		for( size_t i = 0; i < particleStates_.size(); i++ )
		{
			if( i == particleIndex )
			{
				continue;
			}

			forceResult -= getInterconnectionForce( particleIndex, i );
		}

		return forceResult;
	}

	math::Vector ParticleSystem::State::getInterconnectionForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2 ) const
	{
		return forces_[particleIndex1][particleIndex2];
	}

	void ParticleSystem::State::setInterconnectionForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2, const math::Vector& force )
	{
		forces_[particleIndex1][particleIndex2] = force;
		forces_[particleIndex2][particleIndex1] = -force;
	}

	math::Vector ParticleSystem::State::calculateInterconnectionForce( ParticleIndex particleIndex1, ParticleIndex particleIndex2 ) const
	{
		if( particleIndex1 == particleIndex2 )
		{
			return math::ZERO_VECTOR;
		}

		const ParticleState& particleState1 = particleStates_[particleIndex1];
		const ParticleState& particleState2 = particleStates_[particleIndex2];
		const math::Vector distance = particleState1.position - particleState2.position;

		using boost::numeric::ublas::norm_2;
		BOOST_ASSERT_MSG( norm_2( distance ) != 0.0f, "Distance between particles shouldn't be zero!" );

		ParticleSystemConstPtr pSystem = system_.lock();
		if( !pSystem )
		{
			return math::ZERO_VECTOR;
		}

		return pSystem->calculateForce( particleIndex1, particleIndex2, distance );
	}

	math::Vector ParticleSystem::State::getAcceleration( int particleIndex ) const
	{
		ParticleSystemConstPtr pSystem = system_.lock();
		if( pSystem )
		{
			const math::Vector force = getForce( particleIndex );
			return force / pSystem->particles()[particleIndex]->getMass();
		}
		return math::ZERO_VECTOR;
	}

#pragma mark Reading current state

	ParticleState& ParticleSystem::State::getParticleState( int particleIndex )
	{
		BOOST_ASSERT( particleIndex >= 0 );
		BOOST_ASSERT( particleIndex < particleStates_.size() );

		return particleStates_[particleIndex];
	}

	void ParticleSystem::State::getAllStates( std::vector<ParticleState>& states ) const
	{
		states.reserve( particleStates_.size() );
		BOOST_FOREACH( const ParticleState& state, particleStates_ )
		{
			states.push_back( ParticleState( state ) );
		}
	}

	void ParticleSystem::State::print( std::ostream& stream ) const
	{
		int i = 0;
		BOOST_FOREACH( const ParticleState& state, particleStates_ )
		{
			stream << "[N]: " << i++;
			stream << " Pos: " << state.position;
			stream << " Vel: " << state.velocity << std::endl;
		}
	}

	ParticleConstPtr ParticleSystem::State::getParticle( int particleIndex ) const
	{
		ParticleSystemConstPtr pSystem = system_.lock();
		if( pSystem )
		{
			return pSystem->particles()[particleIndex];
		}

		return ParticleConstPtr();
	}

	std::ostream& operator<< ( std::ostream& stream, const SolidSystem::ParticleSystem::State& particle )
	{
		particle.print( stream );
		return stream;
	}
}


