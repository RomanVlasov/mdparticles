﻿#pragma once

#include <boost/numeric/ublas/vector.hpp>

namespace SolidSystem
{
	namespace math
	{
		typedef boost::numeric::ublas::bounded_vector<double, 3> Vector3d;

		typedef Vector3d Vector;

		static const Vector3d ZERO_VECTOR = boost::numeric::ublas::zero_vector<double>( 3 );
	}
}