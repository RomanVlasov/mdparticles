#include "MDPotential.hpp"

namespace SolidSystem
{
	class Particle;

	math::Vector MDPairPotential::calculateGradU( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const
	{
		using namespace boost::numeric;
		const double distanceLength = ublas::norm_2( distance );
		const double uDerivate = calculateUDerivateI( particle1, particle2, distance );
		return distance * ( uDerivate / distanceLength );
	}

//		virtual math::Vector calculateUDerivateI( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const {}
//		virtual math::Vector calculateUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const {}
//		virtual math::Vector calculateMaxUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const {}


//		virtual ~MDPairPotential() {};
//	};

//	typedef boost::shared_ptr<MDPairPotential> MDPairPotentialPtr;
}