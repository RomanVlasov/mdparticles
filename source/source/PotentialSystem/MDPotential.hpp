#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/serialization/assume_abstract.hpp>

#include <source/ParticleSystem/Particle.hpp>
#include <source/SystemDefines.hpp>

BOOST_SERIALIZATION_ASSUME_ABSTRACT(SolidSystem::MDPairPotential);

namespace SolidSystem
{
	class Particle;

	class MDPairPotential
	{
	public:
		math::Vector calculateGradU( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const;

		virtual double calculateUDerivateI( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const = 0;
		virtual double calculateUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const = 0;
		virtual double calculateMaxUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const = 0;

		virtual ~MDPairPotential() {};

	public:
		template<class Archive> 
		void serialize( Archive & ar, const unsigned int version ) {}
	};

	

	typedef boost::shared_ptr<MDPairPotential> MDPairPotentialPtr;
}