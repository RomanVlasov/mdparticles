#include "LennardDjones.hpp"

#include <math.h>
#include <boost/serialization/export.hpp>

BOOST_CLASS_EXPORT(SolidSystem::LennardDjones)

namespace SolidSystem
{
	const double LennardDjones::DEFAULT_CUT_OFF_DISTANCE_COEF = 5.0f;

	LennardDjones::LennardDjones( double absMinValue, double minValueDistance )
	: absMinValue_( absMinValue )
	, minValueDistance_( minValueDistance )
	, minValueDistancePow6_( ::pow( minValueDistance, 6 ) )
	, absMinValue12_( absMinValue * 12.0f )
	{
		setCutOffDistanceCoef( DEFAULT_CUT_OFF_DISTANCE_COEF );
	}

	void LennardDjones::setCutOffDistanceCoef( double coef )
	{
		cutOffDistanceSqr_ = coef * minValueDistance_ * minValueDistance_;
	}

	double LennardDjones::calculateUPotetialEnergy( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const
	{
		using namespace boost::numeric;
		const double distSqrSum = ublas::inner_prod( distance, distance );
		const double tmpCoef = minValueDistancePow6_ / ::pow( distSqrSum, 3 );
		return absMinValue_ * tmpCoef * ( tmpCoef - 2.0f );
	}

	double LennardDjones::calculateUDerivateI( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const
	{
		using namespace boost::numeric;
		const double distSqrSum = ublas::inner_prod( distance, distance );
		if( distSqrSum >= cutOffDistanceSqr_ ) 
		{
			return 0.0f;
		}
	
		const double freeCoef= absMinValue12_ / ::sqrt( distSqrSum );
		const double tmpCoef = minValueDistancePow6_ / ::pow( distSqrSum, 3 );
		return -freeCoef * tmpCoef * ( tmpCoef - 1.0f );
	}

	double LennardDjones::calculateUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const
	{
		//TODO: ����������� �������.
		return 0.0f;
	}

	double LennardDjones::calculateMaxUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const
	{
		return 72.0f * absMinValue_ / ( minValueDistance_ * minValueDistance_ );
	}
}