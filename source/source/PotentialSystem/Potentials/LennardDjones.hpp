#include <source/PotentialSystem/MDPotential.hpp>

namespace SolidSystem
{
	/*
	 �����, ������������ ��������� ��������-������.
	 */
	class LennardDjones : public MDPairPotential
	{
		typedef MDPairPotential base; 

	public:
		static const double DEFAULT_CUT_OFF_DISTANCE_COEF;

	public:
		LennardDjones( double absMinValue, double minValueDistance_ );
		virtual ~LennardDjones() {}

		/*
		 ���������� ����������� ��� ����������, 
		 ������� � �������� �������������� ����� ������������� ������� 
		 �� ������ ���������������� �����
		 */
		void setCutOffDistanceCoef( double coef );

	public:
		virtual double calculateUPotetialEnergy( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const;
		virtual double calculateUDerivateI( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const;
		virtual double calculateUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const;
		virtual double calculateMaxUDerivateII( ParticleConstPtr particle1, ParticleConstPtr particle2, const math::Vector& distance ) const;

	public:
		template<class Archive> 
		void serialize( Archive & ar, const unsigned int version )
		{
			using boost::serialization::make_nvp;
			ar & make_nvp( "base", boost::serialization::base_object<base>( *this ) );
			ar & BOOST_SERIALIZATION_NVP( absMinValue_ );
			ar & BOOST_SERIALIZATION_NVP( minValueDistance_ );
			ar & BOOST_SERIALIZATION_NVP( cutOffDistanceSqr_ );
		}

	private:
		double absMinValue_;
		double minValueDistance_;
		double cutOffDistanceSqr_;

		//���������� ������������ ��� ����������� ����������
		double absMinValue12_;
		double minValueDistancePow6_;
	};
}