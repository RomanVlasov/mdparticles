#include "MDAnimation.hpp"

#include <cpplog/cpplog.hpp>
#include <source/App/App.hpp>

namespace SolidSystem
{
	MDAnimation::MDAnimation( ParticleSystemConstPtr system )
	: system_( system )
	{
	}

	void MDAnimation::addFrame( const ParticleSystem::State& state, double timeFromStart )
	{
		LOG_DEBUG( App::logger() ) << "Time: " << timeFromStart;
		LOG_DEBUG( App::logger() ) << "State: "; 
		LOG_DEBUG( App::logger() ) << state;

		//��������� ����� ��������� ���� ������������ �����
		ParticleStatesPtr states( new ParticleStates() );
		state.getAllStates( *states );

		//��������� ��������� � �������������� �����
		systemStates_.push_back( std::make_pair( states, timeFromStart ) );
	}
}