#pragma once

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/utility.hpp>

#include <source/ParticleSystem/ParticleSystem.hpp>
#include <source/ParticleSystem/ParticleSystemState.hpp>


namespace SolidSystem
{
	/*
	 ����� ������ ������������������ ��������� ������� ������������ ����� � ��������� ������� �������.
	 ����� ������� ��� "��������� ������� ������������ �����"
	 */
	class MDAnimation
	{
	public:
		MDAnimation( ParticleSystemConstPtr system );
		~MDAnimation() {};

	public:
		void addFrame( const ParticleSystem::State& state, double timeFromStart );

	public:
		friend class boost::serialization::access;
		template<class Archive>
		void serialize( Archive & ar, const unsigned int version )
		{
			using boost::serialization::make_nvp;
			ar & make_nvp( "particle_system", system_ );
			ar & make_nvp( "particle_states", systemStates_ );
		}

	private:
		typedef std::vector<ParticleState> ParticleStates;
		typedef boost::shared_ptr<ParticleStates> ParticleStatesPtr;
		typedef std::pair<ParticleStatesPtr, double> SystemStateAtTime;
		typedef std::vector<SystemStateAtTime> SystemStates;
		
		SystemStates systemStates_;
		ParticleSystemConstPtr system_;
	};
}