#include "Dispenser.hpp"

namespace SolidSystem{
namespace calculation
{

	Dispenser::Dispenser( const Config& config )
	: config_( config )
	{
	}

	Dispenser::~Dispenser()
	{
	}


	const Dispenser::Config& Dispenser::config() const
	{
		return config_;
	}
}
}