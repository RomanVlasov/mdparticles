﻿#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>

namespace SolidSystem{
namespace calculation
{
	struct ParticleDistribution;

	/**
	Абстрактный распределитель
	материальных точек между процессорами.
	*/
	class Dispenser
	{
		public:
			typedef int ProcessRank;

			/**
			Конфигурация распределителя
			*/
			struct Config
			{
				 ProcessRank currentProcessor;
				 int totalProcesses;
			};

		public:
			Dispenser( const Config& config );
			virtual ~Dispenser();

		public:
			/**
			Функция возвращает распределение материальных точек
			и пар материальных точек для рассчета силы по процессорам.
			@param distribution В эту структуру будет записываться результат расчета.
			@param matrixDimension Размер квадратной матрицы сил.
			*/
			virtual void getDistribution( ParticleDistribution& distribution, int matrixDimension ) const = 0;

		protected:
			const Config& config() const;

		private:
			Config config_;

	};

	typedef boost::shared_ptr<Dispenser> DispenserPtr;
}
}