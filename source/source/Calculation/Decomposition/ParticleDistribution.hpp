#pragma once

#include <map>

namespace SolidSystem{
namespace calculation
{
	typedef int ProcessorRank;

	template<typename T>
	struct Distribution
	{	
		typedef std::map<ProcessorRank, T> ProcessorValueMap;

		T assigned;
		ProcessorValueMap related;
		int maxAssignedNumber;
	};

	struct ParticleDistribution
	{
		typedef int Index;
		typedef std::vector<Index> Indexes;
		typedef std::pair<Index, Index> Pair;
		typedef std::vector<Pair> Pairs;

		typedef Distribution<Indexes>::ProcessorValueMap ProcessorIndexesMap;

		Distribution<Indexes> particles;
		Distribution<Pairs> particlePairs;
	};
}
}