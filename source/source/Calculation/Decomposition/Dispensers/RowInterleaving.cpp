#include "RowInterleaving.hpp"
#include <source/Calculation/Decomposition/ParticleDistribution.hpp>

namespace SolidSystem {
namespace calculation
{
	namespace
	{
		bool tryAddIndex( ParticleDistribution& distribution, int matrixDimension, int index )
		{
			if( index < matrixDimension )
			{
				distribution.particles.assigned.push_back( index );
				for( int i = index + 1; i < matrixDimension; i++ )
				{
					distribution.particlePairs.assigned.push_back( std::make_pair( index, i ) );
				}

				return true;
			}
			else
			{
				return false;
			}
		}
	}

	RowInterleavingDispenser::RowInterleavingDispenser( const Config& config )
	: base( config )
	{
	}

	void RowInterleavingDispenser::getDistribution( ParticleDistribution& distribution, int matrixDimension ) const
	{
		const int totalProcesses = config().totalProcesses;
		const int currentRank = config().currentProcessor;

		ParticleDistribution::Index index = config().currentProcessor; 
		for( int block = 0;; block += 2 * totalProcesses )
		{
			const ParticleDistribution::Index firstIndex = block + currentRank; 
			if( !tryAddIndex( distribution, matrixDimension, firstIndex ) )
			{
				break;
			}

			const ParticleDistribution::Index secondIndex = block + 2 * totalProcesses - currentRank - 1;
			if( !tryAddIndex( distribution, matrixDimension, secondIndex ) )
			{
				break;
			}
		}

		const double dMatrixDimension = static_cast<double>( matrixDimension );
		const double dTotalProcessors = static_cast<double>( config().totalProcesses );

		/////////////////////////////////////////////////////////////////////////////////////
		// ��� ��������� ����� ��� ������������ �������� �� ��������� �����
		/////////////////////////////////////////////////////////////////////////////////////
		distribution.particles.maxAssignedNumber = static_cast<int>( ::ceil( dMatrixDimension / dTotalProcessors ) );
		distribution.particlePairs.maxAssignedNumber = static_cast<int>( ::ceil( dMatrixDimension * ( dMatrixDimension - 1.0 ) / ( 2.0 * dTotalProcessors ) ) );
	}

}
}
