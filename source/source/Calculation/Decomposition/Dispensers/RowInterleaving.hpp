#pragma once

#include <source/Calculation/Decomposition/Dispenser.hpp>

namespace SolidSystem {
namespace calculation
{
	class RowInterleavingDispenser : public Dispenser
	{
			typedef Dispenser base;

		public:
			RowInterleavingDispenser( const Config& config );
			virtual ~RowInterleavingDispenser() {}

			virtual void getDistribution( ParticleDistribution& distribution, int matrixDimension ) const;
	};
}
}