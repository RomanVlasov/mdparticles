#include "SimulationMethod.hpp"

#include <boost/foreach.hpp>
#include <boost/assert.hpp>

namespace SolidSystem {
namespace calculation
{

	SimulationMethod::SimulationMethod()
	: deltaTime_( -1.0 )
	{
	}


	void SimulationMethod::setDeltaTime( double delta )
	{
		BOOST_ASSERT_MSG( delta > 0.0, "Simulation method delat should be greater then zero" );
		deltaTime_ = delta;

		onDeltaTimeChanged();
	}

	void SimulationMethod::changePosition( ParticleSystem::State& state, ParticleIndex particleIndex )
	{
		doChangePosition( state, particleIndex );
	}

	void SimulationMethod::changePosition( ParticleSystem::State& state, const ParticleIndexes& particleIndexes )
	{
		BOOST_FOREACH( ParticleIndexes::value_type index, particleIndexes )
		{
			changePosition( state, index );
		}
	}

	void SimulationMethod::changeVelocityAndAcceleration( ParticleSystem::State& state, ParticleIndex particleIndex )
	{
		 doChangeVelocityAndAcceleration( state, particleIndex );
	}

	void SimulationMethod::changeVelocityAndAcceleration( ParticleSystem::State& state, const ParticleIndexes& particleIndexes )
	{
		BOOST_FOREACH( ParticleIndexes::value_type index, particleIndexes )
		{
			changeVelocityAndAcceleration( state, index );
		}
	}

	double SimulationMethod::getDeltaTime() const
	{
		return deltaTime_;
	}
}
}