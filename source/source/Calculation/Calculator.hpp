#pragma once

#include <boost/shared_ptr.hpp>
#include "MDParams.hpp"

namespace SolidSystem
{
	class MDAnimation;
	typedef boost::shared_ptr<MDAnimation> MDAnimationPtr;

	struct MDParams;

	class ParticleSystem;
	typedef boost::shared_ptr<ParticleSystem> ParticleSystemPtr;

	namespace calculation
	{
		typedef boost::shared_ptr<class SimulationMethod> SimulationMethodPtr;
		typedef boost::shared_ptr<class Synchronizer> SynchronizerPtr;
		typedef boost::shared_ptr<class Dispenser> DispenserPtr;
	
		/**
		�������� �����, ������������ ��������
		�������
		*/
		class Calculator
		{
		public:
			Calculator();
			~Calculator() {}

			/**
			���������� ��������� ��������� �������
			- �������������� ������������ ����� �� �����������
			- ������������� ����� ������������
			- ��������� �����
			*/
			void setDispenser( DispenserPtr dispenser );
			void setSynchronizer( SynchronizerPtr synchronizer );
			void setMethod( SimulationMethodPtr method );
			void setCalculationParams( const MDParams& params );

			/**
			������ ������� �� ������������ �����
			*/
			void setSystem( ParticleSystemPtr system );

		public:
			/**
			������ ������� � ���������� ��������������� �����������
			*/
			MDAnimationPtr run();

		private:
			std::string getSettingsError() const;
			double advanceTime( double currentTime ) const;
			bool shouldStopCycle( double currentTime ) const;

		private:
			 DispenserPtr dispenser_;
			 SynchronizerPtr synchronizer_;
			 SimulationMethodPtr method_;
			 ParticleSystemPtr system_;
			 MDParams params_;
		};
	}
}