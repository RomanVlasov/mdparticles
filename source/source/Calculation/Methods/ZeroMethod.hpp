#pragma once

#include <vector>
#include <source/Calculation/MDMethod.hpp>
#include <source/ParticleSystem/ParticleSystemState.hpp>
#include <source/ParticleSystem/Particle.hpp>


namespace SolidSystem
{
	class ZeroMethod : public MDMethod
	{
	public:
		ZeroMethod() {}

	private:
		virtual void doInit() {}
		virtual void onStart();
		virtual void advanceState( ParticleSystem::State& state, double deltaTime );
		virtual void save( MDAnimation& output, const ParticleSystem::State& state, double currentTime ) const;
		virtual double advanceTime( double currentTime ) const;
	};
}