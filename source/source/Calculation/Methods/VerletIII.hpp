#pragma once

#include <source/Calculation/SimulationMethod.hpp>

namespace SolidSystem {
namespace calculation
{
	/**
	���������� ���������� ������ ����� � ����� III.
	*/
	class VerletIIIMethod : public SimulationMethod
	{
	public:
		VerletIIIMethod();

	private:
		virtual void onDeltaTimeChanged();
		virtual void doChangePosition( ParticleSystem::State& state, ParticleIndex particleIndex );
		virtual void doChangeVelocityAndAcceleration( ParticleSystem::State& state, ParticleIndex particleIndex );

	private:
		double squaredDeltaTime_;
		double halfedDeltaTime_;
	};
}
}