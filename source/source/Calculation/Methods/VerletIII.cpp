#include "VerletIII.hpp"
#include <source/ParticleSystem/ParticleSystemState.hpp>

namespace SolidSystem {
namespace calculation
{
	VerletIIIMethod::VerletIIIMethod()
	{
	}

	void VerletIIIMethod::onDeltaTimeChanged()
	{
		//////////////////////////////////////////////////////////
		// �� �� ����� ����� ��� ��������� ���� � �� �� ���������.
		// ���, ��� ����� ���� ��������� �������, ������ ���� ��������� �������.
		//////////////////////////////////////////////////////////
		squaredDeltaTime_ = 0.5 * getDeltaTime() * getDeltaTime();
		halfedDeltaTime_ = 0.5 * getDeltaTime();
	}

	void VerletIIIMethod::doChangePosition( ParticleSystem::State& state, ParticleIndex particleIndex )
	{
		ParticleState& particleState = state.getParticleState( particleIndex );

		particleState.position =	particleState.position +
									particleState.velocity * getDeltaTime() +
									particleState.acceleration * squaredDeltaTime_;
	}

	void VerletIIIMethod::doChangeVelocityAndAcceleration( ParticleSystem::State& state, ParticleIndex particleIndex )
	{
		ParticleState& particleState = state.getParticleState( particleIndex );

		math::Vector accelNew = state.getAcceleration( particleIndex );

		particleState.velocity =	particleState.velocity + 
									( accelNew + particleState.acceleration ) * halfedDeltaTime_;
			
		particleState.acceleration = accelNew;
	}
}
}