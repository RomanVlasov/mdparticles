#include "Verlet.hpp"

#include <source/Animation/MDAnimation.hpp>
#include <boost/foreach.hpp>

namespace SolidSystem
{
	void VerletMethod::onStart()
	{
		if( !particleSystem() ) return;

		ParticleSystem::State state( particleSystem() );

		//������� ������ ��������� ������������ �����
		//��������� ����������� ����� � ��������� ������ �������
		accelCache_.resize( particleSystem()->particles().size() );

		for( size_t i = 0; i < particleSystem()->particles().size(); i++ )
		{
			accelCache_[i] = state.getAcceleration( i );
		}
	}

	void VerletMethod::advanceState( ParticleSystem::State& state, double deltaTime )
	{
		if( !particleSystem() ) return;

		for( size_t i = 0; i < particleSystem()->particles().size(); i++ )
		{
			ParticleState& particleState = state.getParticleState( i );

			particleState.position =	particleState.position +
										particleState.velocity * deltaTime +
										accelCache_[i] * 0.5f * deltaTime * deltaTime;
		}

		state.calculateAllForces();

		for( size_t i = 0; i < particleSystem()->particles().size(); i++ )
		{
			ParticleState& particleState = state.getParticleState( i );

			math::Vector accelNew = state.getAcceleration( i );

			particleState.velocity =	particleState.velocity + 
										( accelNew + accelCache_[i] ) * 0.5f * deltaTime;
			
			accelCache_[i] = accelNew;
		}
	}

	void VerletMethod::save( MDAnimation& output, const ParticleSystem::State& state, double currentTime ) const
	{
		output.addFrame( state, currentTime );
	}

	double VerletMethod::advanceTime( double currentTime ) const
	{
		return currentTime + params().timeDelta;
	}
}
