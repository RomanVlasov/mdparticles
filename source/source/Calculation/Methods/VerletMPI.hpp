#include <vector>
#include <source/Calculation/MDMethod.hpp>
#include <source/ParticleSystem/ParticleSystemState.hpp>
#include <source/ParticleSystem/Particle.hpp>

namespace SolidSystem
{
	/*
	����� ��������� ����� ������������ �������� ����� � ����� III
	*/
	class VerletMpiMethod : public MDMethod
	{
	public:
		VerletMpiMethod() {}

	private:
		virtual void doInit() {}
		virtual void onStart();
		virtual void advanceState( ParticleSystem::State& state, double deltaTime );
		virtual void save( MDAnimation& output, const ParticleSystem::State& state, double currentTime ) const;
		virtual double advanceTime( double currentTime ) const;

	private:
		typedef std::vector<math::Vector> AccelerationVector;
		AccelerationVector accelCache_;
	};
}