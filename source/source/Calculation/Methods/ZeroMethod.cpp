#include "ZeroMethod.hpp"

#include <source/Calculation/MDMethod.hpp>
#include <source/Animation/MDAnimation.hpp>
#include <boost/foreach.hpp>

namespace SolidSystem
{
	void ZeroMethod::onStart()
	{
	}

	void ZeroMethod::advanceState( ParticleSystem::State& state, double deltaTime )
	{
		//Do nothing
	}

	void ZeroMethod::save( MDAnimation& output, const ParticleSystem::State& state, double currentTime ) const
	{
		output.addFrame( state, currentTime );
	}

	double ZeroMethod::advanceTime( double currentTime ) const
	{
		return currentTime + params().timeDelta;
	}
}