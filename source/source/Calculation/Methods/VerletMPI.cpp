#include "VerletMpi.hpp"

#include <source/Animation/MDAnimation.hpp>
#include <boost/foreach.hpp>

namespace SolidSystem
{
	void VerletMpiMethod::onStart()
	{
	}

	void VerletMpiMethod::advanceState( ParticleSystem::State& state, double deltaTime )
	{
	}

	void VerletMpiMethod::save( MDAnimation& output, const ParticleSystem::State& state, double currentTime ) const
	{
	}

	double VerletMpiMethod::advanceTime( double currentTime ) const
	{
		return currentTime + params().timeDelta;
	}
}