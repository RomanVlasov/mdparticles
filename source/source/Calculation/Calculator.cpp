#include "Calculator.hpp"

#include <boost/timer/timer.hpp>
#include <boost/foreach.hpp>
#include <cpplog/cpplog.hpp>
#include <source/App/App.hpp>
#include <source/ParticleSystem/ParticleSystem.hpp>
#include <source/Animation/MDAnimation.hpp>

#include "Decomposition/Dispenser.hpp"
#include "Decomposition/ParticleDistribution.hpp"
#include "Synchronizer/Synchronizer.hpp"
#include "SimulationMethod.hpp"

namespace SolidSystem
{
	namespace calculation
	{
		namespace
		{
			void save( MDAnimationPtr output, const ParticleSystem::State& state, double currentTime )
			{
				if( output )
				{
					output->addFrame( state, currentTime );
				}
			}
		}

		
		Calculator::Calculator()
		{
		}

		void Calculator::setDispenser( DispenserPtr dispenser )
		{
			dispenser_ = dispenser;
		}

		void Calculator::setSynchronizer( SynchronizerPtr synchronizer )
		{
			synchronizer_ = synchronizer;
		}

		void Calculator::setMethod( SimulationMethodPtr method )
		{
			method_ = method;
		}

		void Calculator::setCalculationParams( const MDParams& params )
		{
			params_ = params;
		}

		void Calculator::setSystem( ParticleSystemPtr system )
		{
			system_ = system;
		}

		MDAnimationPtr Calculator::run()
		{
			const std::string error = getSettingsError();
			if( !error.empty() )
			{
				LOG_WARN( App::logger() ) << error;
				return MDAnimationPtr();
			}

			boost::shared_ptr<boost::timer::auto_cpu_timer> timer;
			if( synchronizer_->isRootProcessor() )
			{
				timer.reset( new boost::timer::auto_cpu_timer() );
			}

			ParticleDistribution distribution;
			dispenser_->getDistribution( distribution, system_->particles().size() );

			ParticleSystem::State currentState( system_ );
			currentState.calculateForces( distribution.particlePairs.assigned );

			synchronizer_->setDistribution( distribution );
			synchronizer_->synchronizeForceMatrixPairs( currentState );

			///////////////////////////////////////////////////////////////////////////
			// ��������� ������� ��������� ������������ �����
			///////////////////////////////////////////////////////////////////////////
			BOOST_FOREACH( ParticleDistribution::Index index, distribution.particles.assigned )
			{
				ParticleState& particleState = currentState.getParticleState( index );
				particleState.acceleration = currentState.getAcceleration( index );
			}

			double previosTime = params_.startTime;
			double currentTime = advanceTime( previosTime );

			///////////////////////////////////////////////////////////////////////////
			// ������ "�������" ��������� ����� ��������� ������ �� ����.
			///////////////////////////////////////////////////////////////////////////
			MDAnimationPtr output;
			if( synchronizer_->isRootProcessor() )
			{
				output.reset( new MDAnimation( system_ ) );
			}
			save( output, currentState, previosTime );

			method_->setDeltaTime( params_.timeDelta );

			// ���� �� ��������� ������� ������ �� �����...
			while( !shouldStopCycle( currentTime ) )
			{	
				// ������������ ��������� ������� � ��������� ������ �������
				method_->changePosition( currentState, distribution.particles.assigned );

				synchronizer_->synchronizeParticles( currentState );

				currentState.calculateForces( distribution.particlePairs.assigned );

				synchronizer_->synchronizeForceMatrixPairs( currentState );

				method_->changeVelocityAndAcceleration( currentState, distribution.particles.assigned );

				// ��������� ����� ��������� �������
				save( output, currentState, currentTime );

				// ����������� ������
				previosTime = currentTime;
				currentTime = advanceTime( previosTime );
			}

			return output;
		}

		std::string Calculator::getSettingsError() const
		{
			if( !dispenser_ )
			{
				return "Dispenser wasn't set";
			}

			if( !synchronizer_ )
			{
				return "Sycnhronizer wasn't set";
			}

			if( !method_ )
			{
				return "Method wasn't set";
			}

			if( !system_ )
			{
				return "Particle system wasn't set";
			}

			return std::string();
		}

		double Calculator::advanceTime( double currentTime ) const
		{
			return currentTime + params_.timeDelta;
		}

		bool Calculator::shouldStopCycle( double currentTime ) const
		{
			return currentTime >= params_.endTime;
		}
	}
}