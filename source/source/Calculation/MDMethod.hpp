#pragma once

#include <boost/shared_ptr.hpp>
#include <source/ParticleSystem/ParticleSystem.hpp>
#include "MDParams.hpp"


namespace SolidSystem
{
	class MDAnimation;
	typedef boost::shared_ptr<MDAnimation> MDAnimationPtr;

	class MDMethod
	{
	public:
		MDMethod() {}
		virtual ~MDMethod() {}

	protected:
		//��������� ������ �������� �������� �������
		const MDParams& params() const;
		ParticleSystemConstPtr particleSystem() const;

	public:
		void init( ParticleSystemConstPtr system, const MDParams& params );
		MDAnimationPtr run();

	private:
		virtual void doInit() = 0;
		virtual void onStart() = 0;
		virtual void advanceState( ParticleSystem::State& state, double deltaTime ) = 0;
		virtual void save( MDAnimation& output, const ParticleSystem::State& state, double currentTime ) const = 0;
		virtual double advanceTime( double currentTime ) const = 0;

	private:
		bool shouldStopCycle( double currentTime ) const;

	private:
		MDParams params_;
		ParticleSystemConstPtr particleSystem_;
	};

	typedef boost::shared_ptr<MDMethod> MDMethodPtr;
}