#pragma once

namespace SolidSystem
{
	struct MDParams
	{
		MDParams();

		double startTime;
		double endTime;
		double timeDelta;
	};
}