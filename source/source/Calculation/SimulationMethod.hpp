#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include <source/ParticleSystem/ParticleSystem.hpp>

namespace SolidSystem {
namespace calculation
{
	/**
	����������� ����� ��� ����������
	���������� ������.
	������������� ��������� ��� ����� �������, �������� � ���������
	������������ ����� � ������ ���������� ������.
	*/
	class SimulationMethod
	{
	public:
		typedef int ParticleIndex;
		typedef std::vector<ParticleIndex> ParticleIndexes;

	public:
		SimulationMethod();
		virtual ~SimulationMethod() {}

		/**
		����� ���� ������
		*/
		void setDeltaTime( double delta );

		void changePosition( ParticleSystem::State& state, ParticleIndex particleIndex );
		void changePosition( ParticleSystem::State& state, const ParticleIndexes& particleIndexes );
		void changeVelocityAndAcceleration( ParticleSystem::State& state, ParticleIndex particleIndex );
		void changeVelocityAndAcceleration( ParticleSystem::State& state, const ParticleIndexes& particleIndexes );

	public:
		double getDeltaTime() const;

	protected:
		virtual void onDeltaTimeChanged() = 0;
		virtual void doChangePosition( ParticleSystem::State& state, ParticleIndex particleIndex ) = 0;
		virtual void doChangeVelocityAndAcceleration( ParticleSystem::State& state, ParticleIndex particleIndex ) = 0;

	private:
		double deltaTime_;
	};
}
}