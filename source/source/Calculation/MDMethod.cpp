#include "MDMethod.hpp"
#include <source/Animation/MDAnimation.hpp>

namespace SolidSystem
{

#pragma mark Access to core parameters

	const MDParams& MDMethod::params() const
	{
		return params_;
	}

	ParticleSystemConstPtr MDMethod::particleSystem() const
	{
		return particleSystem_;
	}


#pragma mark Main functionality

	bool MDMethod::shouldStopCycle( double currentTime ) const
	{
		return currentTime >= params().endTime;
	}

	void MDMethod::init( ParticleSystemConstPtr system, const MDParams& params )
	{
		particleSystem_ = system;
		params_ = params;
	}

	MDAnimationPtr MDMethod::run()
	{
		if( !particleSystem() ) return MDAnimationPtr();

		// ������� ������ ��������
		// � ��� ����� ��������� ��������� ������� 
		// � ��������� ������� �������
		MDAnimationPtr output( new MDAnimation( particleSystem() ) );

		// ��������� ��������� ������� � ������ ������ �������
		ParticleSystem::State currentState( particleSystem_ );

		// ������ ����������� �����������, �������������� ����������
		onStart();

		// ��������� ��������� ��������� �������
		double previosTime = params().startTime;
		double currentTime = advanceTime( previosTime );
		save( *output, currentState, previosTime );

		// ���� �� ��������� ������� ������ �� �����...
		while( !shouldStopCycle( currentTime ) )
		{	
			// ������������ ��������� ������� � ��������� ������ �������
			const double deltaTime = currentTime - previosTime;
			advanceState( currentState, deltaTime );

			// ��������� ����� ��������� �������
			save( *output, currentState, currentTime );

			// ����������� ������
			previosTime = currentTime;
			currentTime = advanceTime( previosTime );
		}

		return output;
	}
}