#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/mpi/communicator.hpp>

#include <source/ParticleSystem/ParticleSystemState.hpp>
#include <source/Calculation/Decomposition/ParticleDistribution.hpp>

#include "Mpi/MessageData.hpp"


namespace SolidSystem {
namespace calculation
{
	struct ParticleDistribution;

	/*
	�����, ���������������� ������������ ����� � ���� ����� ������������
	*/
	class Synchronizer
	{
	public:
		Synchronizer();
		virtual ~Synchronizer() {}

		void setDistribution( const ParticleDistribution& distribution );
		void synchronizeParticles( ParticleSystem::State& systemState );
		void synchronizeForceMatrixPairs( ParticleSystem::State& systemState );

	public:
		bool isRootProcessor() const;
		int getProcessorsNumber() const;
		int getCurrentProcessor() const;

	private:
		ParticleDistribution distribution_;
		boost::mpi::communicator communicator_;

		ParticlePositionInfo::Vector particleMessageBuffer_;
		ParticlePositionInfo::Vector particleReceiveBuffer_;
		ForceMatrixEntryInfo::Vector forceMessageBuffer_;
		ForceMatrixEntryInfo::Vector forceReceiveBuffer_;
	};

	typedef boost::shared_ptr<Synchronizer> SynchronizerPtr;
}
}