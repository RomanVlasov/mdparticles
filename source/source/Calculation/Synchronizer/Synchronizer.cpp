#include "Synchronizer.hpp"

#include <boost/mpi.hpp>
#include <boost/foreach.hpp>
#include <cpplog/cpplog.hpp>

#include <algorithm>
#include <iterator>

#include <source/Calculation/Decomposition/ParticleDistribution.hpp>
#include <source/SystemDefines.hpp>



namespace SolidSystem {
namespace calculation
{	
	Synchronizer::Synchronizer()
	{
	}

	void Synchronizer::setDistribution( const ParticleDistribution& distribution ) 
	{
		distribution_ = distribution;
	}

	void Synchronizer::synchronizeParticles( ParticleSystem::State& systemState ) 
	{
		particleMessageBuffer_.clear();
		particleReceiveBuffer_.clear();

		////////////////////////////////////////////////////////////
		// ��������� �����, ������� ��� ����� ��������
		////////////////////////////////////////////////////////////
		BOOST_FOREACH( int index, distribution_.particles.assigned )
		{
			////////////////////////////////////////////////////////////
			// TODO: ��������, �� ����� �� ������� �������� ��������� � ������,
			// � ����� ��������� ��
			////////////////////////////////////////////////////////////
			ParticlePositionInfo info;
			info.particleIndex = index;
			info.position = systemState.getParticleState( index ).position;
			particleMessageBuffer_.push_back( info );
		}

		BOOST_ASSERT_MSG( distribution_.particles.maxAssignedNumber >= particleMessageBuffer_.size(), 
			"Some error in calculation max assigned number of particles occured" );

		particleMessageBuffer_.resize( distribution_.particles.maxAssignedNumber );

		////////////////////////////////////////////////////////////
		// �������� �������� �� ���� ��������� �����
		////////////////////////////////////////////////////////////
		namespace mpi = boost::mpi;
		mpi::all_gather( communicator_, particleMessageBuffer_.data(), particleMessageBuffer_.size(), particleReceiveBuffer_ );
		
		////////////////////////////////////////////////////////////
		// ���������� � ����� ���������� ������
		////////////////////////////////////////////////////////////
		BOOST_FOREACH( const ParticlePositionInfo::Vector::value_type& value, particleReceiveBuffer_ )
		{
			if( value.particleIndex != INDEX_UNDEFINED )
			{
				ParticleState& state = systemState.getParticleState( value.particleIndex );
				state.position = value.position;
			}
		}
	}

	void Synchronizer::synchronizeForceMatrixPairs( ParticleSystem::State& systemState ) 
	{
		forceMessageBuffer_.clear();
		forceReceiveBuffer_.clear();

		////////////////////////////////////////////////////////////
		// ��������� �����, ������� ��� ����� ��������
		////////////////////////////////////////////////////////////
		BOOST_FOREACH( const ParticleDistribution::Pairs::value_type& value, distribution_.particlePairs.assigned )
		{
			////////////////////////////////////////////////////////////
			// TODO: ��������, �� ����� �� ������� �������� ��������� � ������,
			// � ����� ��������� ��
			////////////////////////////////////////////////////////////
			ForceMatrixEntryInfo info;
			info.particleIndex1 = value.first;
			info.particleIndex2 = value.second;
			info.force = systemState.getInterconnectionForce( value.first, value.second );
			forceMessageBuffer_.push_back( info );
		}

		BOOST_ASSERT_MSG( distribution_.particlePairs.maxAssignedNumber >= forceMessageBuffer_.size(), 
			"Some error in calculation max assigned number of particle pairs occured" );

		forceMessageBuffer_.resize( distribution_.particlePairs.maxAssignedNumber );

		////////////////////////////////////////////////////////////
		// �������� �������� �� ���� ��������� �����
		////////////////////////////////////////////////////////////
		namespace mpi = boost::mpi;
		mpi::all_gather( communicator_, forceMessageBuffer_.data(), forceMessageBuffer_.size(), forceReceiveBuffer_ );
		
		////////////////////////////////////////////////////////////
		// ���������� � ����� ���������� ������
		////////////////////////////////////////////////////////////
		BOOST_FOREACH( const ForceMatrixEntryInfo::Vector::value_type& value, forceReceiveBuffer_ )
		{
			if( value.particleIndex1 != INDEX_UNDEFINED )
			{
				systemState.setInterconnectionForce( value.particleIndex1, value.particleIndex2, value.force );
			}
		}
	}

	bool Synchronizer::isRootProcessor() const
	{
		static const int ROOT_RANK = 0;
		return getCurrentProcessor() == ROOT_RANK;
	}

	int Synchronizer::getProcessorsNumber() const 
	{ 
		return communicator_.size(); 
	}

	int Synchronizer::getCurrentProcessor() const 
	{ 
		return communicator_.rank(); 
	}

}
}