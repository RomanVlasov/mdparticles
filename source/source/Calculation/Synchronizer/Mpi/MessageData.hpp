#pragma once

#include <vector>
#include <boost/mpi/datatype.hpp>
#include <source/SystemDefines.hpp>

namespace SolidSystem {
namespace calculation
{
	enum
	{
		INDEX_UNDEFINED = -1
	};

	/**
	��������������� ��������� ������ ��� ��������� ������� ������������ �����
	����� ���������� ������ � ������� ���������� MPI
	*/
	struct ParticlePositionInfo
	{
		typedef std::vector<ParticlePositionInfo> Vector;	

		ParticlePositionInfo();

		int particleIndex;
		SolidSystem::math::Vector position;

		template<class Archive>
		void serialize( Archive & ar, const unsigned int version )
		{
			using boost::serialization::make_nvp;
			ar & particleIndex;
			ar & position;
		}
	};

	/**
	��������������� ��������� ������ ��� ��������� ������ �� 
	�������� ������� ��� ����� ���������� ������ � ������� ���������� MPI.
	*/
	struct ForceMatrixEntryInfo
	{
		typedef std::vector<ForceMatrixEntryInfo> Vector;

		ForceMatrixEntryInfo();

		int particleIndex1;
		int particleIndex2;
		SolidSystem::math::Vector force;

		template<class Archive>
		void serialize( Archive & ar, const unsigned int version )
		{
			using boost::serialization::make_nvp;
			ar & particleIndex1;
			ar & particleIndex2;
			ar & force;
		}
	};
}
}

BOOST_IS_MPI_DATATYPE( SolidSystem::calculation::ParticlePositionInfo );
BOOST_IS_MPI_DATATYPE( SolidSystem::calculation::ForceMatrixEntryInfo );