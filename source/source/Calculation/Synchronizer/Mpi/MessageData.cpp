#include "MessageData.hpp"

namespace SolidSystem {
namespace calculation
{
	ParticlePositionInfo::ParticlePositionInfo()
	: particleIndex( INDEX_UNDEFINED )
	{
	}


	ForceMatrixEntryInfo::ForceMatrixEntryInfo()
	: particleIndex1( INDEX_UNDEFINED )
	, particleIndex2( INDEX_UNDEFINED )
	{
	}
}
}