#pragma once

//#include <boost/archive/detail/common_oarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include <json_spirit/json_spirit_value.h>
#include "JsonValueConverter.hpp"

namespace SolidSystem {
namespace serialization
{
	/**
	Specific archive to read data from json files
	*/
	class JsonOutputArchive// : public boost::archive::detail::common_oarchive<JsonOutputArchive>
	{
		typedef JsonOutputArchive SelfType;

	public:
		JsonOutputArchive( json_spirit::Object& object, unsigned int version )
			: object_( object )
			, version_( version )
		{}


	protected:
		// permit serialization system privileged access to permit
		// implementation of inline templates for maximum speed.
		//friend class boost::archive::detail::interface_oarchive<JsonOutputArchive>;
		//friend class boost::archive::save_access;

		// member template for loading primitive types.
		// Override for any types/templates that special treatment
		/*template<class T>
		void save( T & t )
		{
			save( "1", t );
		}*/

		//template<class T>
		//void save_override(T & t, BOOST_PFTO int version)
		//{
		//	// If your program fails to compile here, its most likely due to
		//	// not specifying an nvp wrapper around the variable to
		//	// be serialized.
		//	BOOST_MPL_ASSERT((boost::serialization::is_wrapper< T >));
		//	this->base::save_override(t, version);
		//}

		//template<class T>
		//void save_override(T & t, BOOST_PFTO int version)
		//{
		//	// If your program fails to compile here, its most likely due to
		//	// not specifying an nvp wrapper around the variable to
		//	// be serialized.
		//	BOOST_MPL_ASSERT((boost::serialization::is_wrapper< T >));
		//	this->base::save_override(t, version);
		//}
		//template<class T>


		template<class T>
		void save( const boost::serialization::nvp<T>& t )
		{
			save( t.name(), t.value() );
		}

		template<class T>
		void save( const char* name, T& value )
		{
			JsonValueConverter converter;
			object_.push_back( json_spirit::Pair( name, converter.convert( value ) ) );
		}

		template<class T>
		void save_primitive_value( const char* name, T& value )
		{
			object_.push_back( json_spirit::Pair( name, value ) );
		}

		/*template<class T>
		void save( const char* name, boost::shared_ptr<T> value )
		{
			if( t )
			{
				t->serialize<SelfType>( *this, version_ );
			}
		}*/

		void save( const char* name, int value );
		void save( const char* name, double value );
		void save( const char* name, bool value );
		void save( const char* name, const char* value );
		void save( const char* name, const std::string& value );

		/*template<typename T, template <typename, typename> class Container>
		void save( const char* name, const Container<T, std::allocator<T>>& value )
		{
			using namespace json_spirit;
			object_.push_back( Pair( name, Value( value.begin(), value.end() ) ) );
		}
*/
	public:
		template<class T>
		void operator &( const boost::serialization::nvp<T>& t )
		{
			save( t );
		}

		template<class T>
		void operator <<( const T& t )
		{
			(*this) & t;
		}

	public:
		//////////////////////////////////////////////////////////
		// public interface used by programs that use the
		// serialization library

		// archives are expected to support this function
		//void load_binary(void *address, std::size_t count);

	private:
		json_spirit::Object& object_;
		unsigned int version_;
	};

}
}