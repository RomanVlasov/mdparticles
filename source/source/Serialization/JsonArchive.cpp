#include "JsonArchive.hpp"

namespace SolidSystem {
namespace serialization
{
	void JsonOutputArchive::save( const char* name, int value )
	{
		save_primitive_value( name, value );
	}

	void JsonOutputArchive::save( const char* name, double value )
	{
		save_primitive_value( name, value );
	}

	void JsonOutputArchive::save( const char* name, bool value )
	{
		save_primitive_value( name, value );
	}

	void JsonOutputArchive::save( const char* name, const char* value )
	{
		save_primitive_value( name, value );
	}

	void JsonOutputArchive::save( const char* name, const std::string& value )
	{
		save_primitive_value( name, value );
	}
}
}