#pragma once

#include <boost/shared_ptr.hpp>
#include <json_spirit/json_spirit_value.h>
#include <source/SystemDefines.hpp>

namespace SolidSystem {
namespace serialization
{
	class JsonOutputArchive;

	class JsonValueConverter
	{
	public:
		template<class T>
		json_spirit::Value convert( const T& t )
		{
			json_spirit::Object newObject;

			JsonOutputArchive archive( newObject, 0 );
			const_cast<T&>(t).serialize( archive, 0 );

			return newObject;
		}

		template<class T>
		json_spirit::Value convert( const std::vector<T>& t )
		{
			return convert_container( t );
		}

		template<typename T1, typename T2>
		json_spirit::Value convert( const std::pair<T1, T2>& t )
		{
			std::vector<json_spirit::Value> values( 2 );
			values[0] = convert( t.first );
			values[1] = convert( t.second );

			return json_spirit::Value( values.begin(), values.end() );
		}

		template<typename T>
		json_spirit::Value convert_container( const T & t )
		{
			typedef T TContainer;

			std::vector<json_spirit::Value> values;
			values.resize( t.size() );

			for( int i = 0; i < t.size(); i++ )
			{			
				///////////////////////////////////////////////
				// ����������� �������� ����������� �������
				// bind �� ��������
				///////////////////////////////////////////////
				values[i] = convert( t[i] );
			}

			return json_spirit::Value( values.begin(), values.end() );
		}

		template<typename T>
		json_spirit::Value convert( boost::shared_ptr<T> t )
		{
			if( !t )
			{
				return json_spirit::Value();
			}

			return convert( *t );
		}

		json_spirit::Value convert( const math::Vector& t )
		{
			return convert_container( t );
		}

		template<typename T>
		json_spirit::Value convertPrimitive( const T& t )
		{
			return json_spirit::Value( t );
		}

		json_spirit::Value convert( int value )
		{
			return convertPrimitive( value );
		}

		json_spirit::Value convert( unsigned int value )
		{
			return convertPrimitive( static_cast<int>( value ) );
		}

		json_spirit::Value convert( double value )
		{
			return convertPrimitive( value );
		}

		json_spirit::Value convert( bool value )
		{
			return convertPrimitive( value );
		}

		json_spirit::Value convert( const char* value )
		{
			return convertPrimitive( value );
		}

		json_spirit::Value convert( const std::string& value )
		{
			return convertPrimitive( value );
		}

	};

}
}