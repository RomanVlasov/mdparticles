#include "App.hpp"

#include <boost/shared_ptr.hpp>
#include <boost/mpi/environment.hpp>
#include <cpplog/cpplog.hpp>

namespace SolidSystem
{
	////////////////////////////////////////////////////////////////
	// ������� ���������� ������ App
	// ������ pImpl
	////////////////////////////////////////////////////////////////
	struct App::Implementation
	{
		boost::shared_ptr<cpplog::OstreamLogger> logger;
		boost::shared_ptr<boost::mpi::environment> mpiEnvironment;

		Implementation();
	};

	App::Implementation::Implementation()
	: logger( new cpplog::StdErrLogger() )
	{
	}

	App::ImplPtr App::pImpl( new Implementation() );

	////////////////////////////////////////////////////////////////
	// ������������� ��������� �����
	////////////////////////////////////////////////////////////////
	void App::setupEnvironment( int argc, char* argv [] )
	{
		pImpl->mpiEnvironment.reset( new boost::mpi::environment( argc, argv ) );
	}

	void App::finish()
	{
		pImpl->mpiEnvironment.reset();
	}

	////////////////////////////////////////////////////////////////
	// ������ � �����
	////////////////////////////////////////////////////////////////
	cpplog::OstreamLogger& App::logger()
	{
		return *pImpl->logger;
	}

	void App::setLogger( LogType logType )
	{
		//TODO: Add log types
	}
}