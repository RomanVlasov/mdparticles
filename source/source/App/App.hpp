#pragma once

#include <boost/scoped_ptr.hpp>

namespace cpplog
{
	class OstreamLogger;
}

namespace SolidSystem
{
	/**
	����������� ����� ����������
	*/
	class App
	{
	public:
		enum LogType
		{
			LOG_STDERR,
			LOG_FILE
		};

	public:
		/**
		� ���� ������� ����� �����������,
		� ����� ����� �������� ����������,
		����� ����������� �������� � ����� ��������� 
		�������� ������������ � ��������� ������.
		*/
		static void setupEnvironment( int argc, char* argv [] );
		static void finish();

		/**
		������ � ����������� ����.
		����� �������� ������ ��� �����������. 
		��. ���������� cpplog.
		*/
		static cpplog::OstreamLogger& logger();
		static void setLogger( LogType logType );

	private:
		struct Implementation;
		typedef boost::scoped_ptr<Implementation> ImplPtr;
		static ImplPtr pImpl;
	};

}