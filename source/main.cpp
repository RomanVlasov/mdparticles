#include <source/App/App.hpp>
#include <examples/TwoParticlesExample.hpp>
#include <examples/TenParticlesExample.hpp>


/**
����� ����� � ����������.
TODO: ��������� ������� ����� ��������� ������.
*/
int main(int argc, char* argv[])
{
	SolidSystem::App::setupEnvironment( argc, argv );

	///////////////////////////////////////////////////////////////
	// ���� ��� ������ ������� ���������� ���������
	///////////////////////////////////////////////////////////////
	{
		using namespace SolidSystem::examples;

		//ExampleBasePtr example( new TwoParticlesExample() );
		ExampleBasePtr example( new TenParticlesExample() );
		example->run();
	}

	SolidSystem::App::finish();

	system("Pause");
    return 0;
}

