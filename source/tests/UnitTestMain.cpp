#include "targetver.h"
#include <mpi.h>
#include <iostream>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <gtest/gtest.h>


TEST(sample_test_case, sample_test)
{
    EXPECT_EQ(1, 1);
}

int main(int argc, char* argv[])
{	
	boost::mpi::environment env( argc, argv );
	boost::mpi::communicator communicator;

	/*if( communicator.rank() > 1  )
	{
		return 0;
	}*/
	
	testing::InitGoogleTest(&argc, argv); 
	int result = RUN_ALL_TESTS();
	system("Pause");
	return result;
}