#pragma once

namespace SolidSystem {

namespace tests
{
	void runParticleSimpleTest();

	void runParticleSystemTest();

	void runSimulationTest();
}

}