#include "gtest/gtest.h"

#include <sstream>

#include <boost/numeric/ublas/traits.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <ParticleSystem/Particle.hpp>

TEST( ParticleStateSerialization, Binary )
{
	using namespace SolidSystem;

	math::Vector TEST_VELOCITY;
	TEST_VELOCITY[0] = 0.0;
	TEST_VELOCITY[1] = 2.0;
	TEST_VELOCITY[2] = 5.0;

	Particle::State state;
	state.position = math::ZERO_VECTOR;
	state.velocity = TEST_VELOCITY;

	std::stringstream save;
	boost::archive::binary_oarchive oarchive( save );
	oarchive << boost::serialization::make_nvp( "test_root", state );

	boost::archive::binary_iarchive iarchive( save );
	Particle::State state2;
	iarchive >> boost::serialization::make_nvp( "test_root", state2 );

	using namespace boost::numeric;
	EXPECT_EQ( 0.0f, ublas::norm_2( math::ZERO_VECTOR - state2.position ) );
	EXPECT_EQ( 0.0f, ublas::norm_2( TEST_VELOCITY - state2.velocity ) );

}