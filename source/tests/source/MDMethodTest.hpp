#pragma once

namespace SolidSystem
{
	class MDAnimation;

	namespace tests
	{
		class TestBase
		{
		public:
			virtual ~TestBase() {}

		public:
			void run() { doRun(); }

		private:
			virtual void doRun() = 0;
		};

		class MDMethodTest : public TestBase
		{
		public:
			MDMethodTest();
			virtual ~MDMethodTest();

		private:
			void testZeroMethod();
			void testVerletMethod();

			void testSaveData( const MDAnimation& animation );
			void testLoadData( MDAnimation& animation );

		private:
			virtual void doRun();

		};
	}
}