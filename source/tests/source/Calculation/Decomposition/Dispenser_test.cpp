﻿#include "gtest/gtest.h"

#include <vector>
#include <boost/foreach.hpp>
#include <boost/range/size.hpp>

#include <source/Calculation/Decomposition/Dispensers/RowInterleaving.hpp>
#include <source/Calculation/Decomposition/ParticleDistribution.hpp>

//TEST( RowInterleavingTest, Constructor )
//{
//	using namespace SolidSystem::calculation;
//	EXPECT_DEATH( { RowInterleavingDispenser dispenser( 5, 1 ); }, "Wrong processor parameters" );
//	EXPECT_DEATH( { RowInterleavingDispenser dispenser( 30, 30 ); }, "Wrong processor parameters" );
//	EXPECT_DEATH( { RowInterleavingDispenser dispenser( 0, 0 ); }, "Wrong processor parameters" );
//}

TEST( RowInterleavingTest, NoMatrix )
{
	using namespace SolidSystem::calculation;
	const int totalProcessors = 1;
	const int currentProcessor = 0;

	Dispenser::Config config = { currentProcessor, totalProcessors };

	RowInterleavingDispenser dispenser( config );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, 0 );

	EXPECT_TRUE( distribution.particlePairs.assigned.empty() );
	EXPECT_TRUE( distribution.particles.assigned.empty() );
	EXPECT_TRUE( distribution.particlePairs.related.empty() );
	EXPECT_TRUE( distribution.particles.related.empty() );
}

TEST( RowInterleavingTest, SingleProcessor )
{
	using namespace SolidSystem::calculation;
	const int totalProcessors = 1;
	const int currentProcessor = 0;
	const int squareMatrixDimension = 4;

	Dispenser::Config config = { currentProcessor, totalProcessors };

	RowInterleavingDispenser dispenser( config );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	EXPECT_EQ( 4, distribution.particles.assigned.size() );
	EXPECT_EQ( 3 + 2 + 1, distribution.particlePairs.assigned.size() );
	EXPECT_TRUE( distribution.particles.related.empty() );
	EXPECT_TRUE( distribution.particlePairs.related.empty() );
}

TEST( RowInterleavingTest, SecondProcessor )
{
	using namespace SolidSystem::calculation;
	const int totalProcessors = 2;
	const int currentProcessor = 1;
	const int squareMatrixDimension = 4;

	Dispenser::Config config = { currentProcessor, totalProcessors };

	RowInterleavingDispenser dispenser( config );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	const ParticleDistribution::Pairs& assignedPairs = distribution.particlePairs.assigned;

	EXPECT_EQ( 2, distribution.particles.assigned.size() );
	EXPECT_EQ( 2 + 1, assignedPairs.size() );
	EXPECT_NE( std::find( assignedPairs.begin(), assignedPairs.end(), std::make_pair( 2, 3 ) ), assignedPairs.end() );
}

TEST( RowInterleavingTest, MaxAssignedNumber2 )
{
	using namespace SolidSystem::calculation;
	const int totalProcessors = 2;
	const int currentProcessor = 1;
	const int squareMatrixDimension = 5;

	Dispenser::Config config = { currentProcessor, totalProcessors };

	RowInterleavingDispenser dispenser( config );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	const ParticleDistribution::Pairs& assignedPairs = distribution.particlePairs.assigned;

	EXPECT_EQ( 3, distribution.particles.maxAssignedNumber );
	EXPECT_EQ( 5, distribution.particlePairs.maxAssignedNumber );
}

TEST( RowInterleavingTest, MaxAssignedNumber3 )
{
	using namespace SolidSystem::calculation;
	const int totalProcessors = 3;
	const int currentProcessor = 1;
	const int squareMatrixDimension = 5;

	Dispenser::Config config = { currentProcessor, totalProcessors };

	RowInterleavingDispenser dispenser( config );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	const ParticleDistribution::Pairs& assignedPairs = distribution.particlePairs.assigned;

	EXPECT_EQ( 2, distribution.particles.maxAssignedNumber );
	EXPECT_EQ( 4, distribution.particlePairs.maxAssignedNumber );
}

class RowInterleavingMultiProcessor : public testing::Test
{
public:
	RowInterleavingMultiProcessor()
	: dispenser( SolidSystem::calculation::Dispenser::Config() )
	{
	}

	virtual void SetUp()
	{
		
	}

	void setProcessor( int rank )
	{
		using namespace SolidSystem::calculation;
		Dispenser::Config config =  { rank, totalProcessors };
		dispenser = RowInterleavingDispenser( config );
	}

	static const int totalProcessors = 3;
	static const int squareMatrixDimension = 6;
	SolidSystem::calculation::RowInterleavingDispenser dispenser;
};

TEST_F( RowInterleavingMultiProcessor, AssignedToFirst  )
{
	using namespace SolidSystem::calculation;

	setProcessor( 0 );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	//////////////////////////////////////////////////////////////////////////////////////////
	// Проверим назначенные первому процессору пары
	//////////////////////////////////////////////////////////////////////////////////////////
	const ParticleDistribution::Pairs& assignedPairs = distribution.particlePairs.assigned;
	const ParticleDistribution::Pair assignedPairsToCheck [] = 
	{
		std::make_pair( 0, 1 ),
		std::make_pair( 0, 2 ),
		std::make_pair( 0, 3 ),
		std::make_pair( 0, 4 ),
		std::make_pair( 0, 5 )
	};

	EXPECT_EQ( assignedPairs.size(), boost::size( assignedPairsToCheck ) );
	BOOST_FOREACH( ParticleDistribution::Pair pair, assignedPairsToCheck )
	{
		EXPECT_NE( std::find( assignedPairs.begin(), assignedPairs.end(), pair ), assignedPairs.end() );
	}
}

TEST_F( RowInterleavingMultiProcessor, AssignedToSecond  )
{
	using namespace SolidSystem::calculation;
	setProcessor( 1 );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	//////////////////////////////////////////////////////////////////////////////////////////
	// Проверим назначенные второму процессору пары
	//////////////////////////////////////////////////////////////////////////////////////////
	const ParticleDistribution::Pairs& assignedPairs = distribution.particlePairs.assigned;
	const ParticleDistribution::Pair assignedPairsToCheck [] = 
	{
		std::make_pair( 1, 2 ),
		std::make_pair( 1, 3 ),
		std::make_pair( 1, 4 ),
		std::make_pair( 1, 5 ),
		std::make_pair( 4, 5 )
	};

	EXPECT_EQ( assignedPairs.size(), boost::size( assignedPairsToCheck ) );
	BOOST_FOREACH( ParticleDistribution::Pair pair, assignedPairsToCheck )
	{
		EXPECT_NE( std::find( assignedPairs.begin(), assignedPairs.end(), pair ), assignedPairs.end() );
	}
}

TEST_F( RowInterleavingMultiProcessor, MaxAssignedNumberFirstProcessor )
{
	using namespace SolidSystem::calculation;

	setProcessor( 0 );
	ParticleDistribution distribution;
	dispenser.getDistribution( distribution, squareMatrixDimension );

	EXPECT_EQ( 2, distribution.particles.maxAssignedNumber );
	EXPECT_EQ( 5, distribution.particlePairs.maxAssignedNumber );
}

TEST_F( RowInterleavingMultiProcessor, Related  )
{
	//////////////////////////////////////////////////////////////////////////////////////////
	// Проверим пары, связанные с другим процессором
	//////////////////////////////////////////////////////////////////////////////////////////

	/*const Dispenser::Pairs& relatedPairs = distribution.particlePairs.related;
	const Dispenser::Pair relatedPairsToCheck [] = 
	{
		std::make_pair( 1, 4 ),
		std::make_pair( 2, 4 ),
		std::make_pair( 3, 4 ),
		std::make_pair( 4, 5 )
	};

	EXPECT_EQ( relatedPairs.size(), boost::size( relatedPairsToCheck ) );
	BOOST_FOREACH( Dispenser::Pair pair, relatedPairsToCheck )
	{
		EXPECT_NE( std::find( relatedPairs.begin(), relatedPairs.end(), pair ),  relatedPairs.end() );
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// �������� ����������� �����
	//////////////////////////////////////////////////////////////////////////////////////////
	const Dispenser::Indexes& assignedIndexes = distribution.particles.assigned;
	const int assignedIndexesToCheck [] = { 0, 4 };
	EXPECT_EQ( assignedIndexes.size(), boost::size( assignedIndexesToCheck ) );
	BOOST_FOREACH( int index, assignedIndexesToCheck )
	{
		EXPECT_NE( std::find( assignedIndexes.begin(), assignedIndexes.end(), index ), assignedIndexes.end() );
	}*/

	//////////////////////////////////////////////////////////////////////////////////////////
	// �������� ��������� �����
	//////////////////////////////////////////////////////////////////////////////////////////
	/*const Dispenser::Indexes& relatedIndexes = distribution.particles.related[];
	const int relatedIndexesToCheck [] = { 1, 2, 3, 5 };
	EXPECT_EQ( relatedIndexes.size(), boost::size( relatedIndexesToCheck ) );
	BOOST_FOREACH( int index, relatedIndexesToCheck )
	{
		EXPECT_NE( std::find( relatedIndexes.begin(), relatedIndexes.end(), index ), relatedIndexes.end() );
	}*/

}
