﻿#include "gtest/gtest.h"

#include <boost/foreach.hpp>

#include <source/Calculation/Synchronizer/ForceMatrix.hpp>
#include <source/Calculation/Decomposition/ParticleDistribution.hpp>
#include <source/Calculation/Decomposition/Dispenser.hpp>
#include <source/ParticleSystem/ParticleSystemState.hpp>
#include <source/ParticleSystem/ParticleSystem.hpp>
#include <source/PotentialSystem/Potentials/LennardDjones.hpp>

static SolidSystem::math::Vector VECTOR( double x, double y, double z )
{
	SolidSystem::math::Vector vec;
	vec.data()[0] = x;
	vec.data()[1] = y;
	vec.data()[2] = z;
	return vec;
}


TEST( SynchronizerConstructorTest, Constructor )
{
	using namespace SolidSystem::calculation;
	using namespace SolidSystem;

	//ParticleSystem::State state;

	ForceMatrixSynchronizer synchronizer;
	//synchronizer.synchronize(  );

}

class SynchronizerTest : public testing::Test
{
	struct ParticleParam
	{
		SolidSystem::math::Vector position;
		SolidSystem::Particle::MassType mass;
	};

public:

	int getTotalProcessors() const
	{
		using namespace SolidSystem::calculation;
		ForceMatrixSynchronizer synchronizer;
		return synchronizer.getProcessorsNumber();
	}

	int getCurrentProcessor() const
	{
		using namespace SolidSystem::calculation;
		ForceMatrixSynchronizer synchronizer;
		return synchronizer.getCurrentProcessor();
	}

	virtual void SetUp()
	{
		using namespace SolidSystem::calculation;
		using namespace SolidSystem;

		///////////////////////////////////////////////////////////////////////////////
		// Определяем систему частиц
		///////////////////////////////////////////////////////////////////////////////
		const ParticleParam params [] = 
		{
			{ VECTOR( 0.0f, 0.0f, 0.0f ), 1.0f },
			{ VECTOR( 0.0f, 0.0f, 1.0f ), 1.0f }
		};

		particleSystem = ParticleSystemPtr( new ParticleSystem() );
		BOOST_FOREACH( const ParticleParam& param, params )
		{
			particleSystem->addParticle( ParticlePtr( new Particle( param.position, param.mass ) ) );
		}
		particleSystem->setPotential( 0, 1, MDPairPotentialPtr( new LennardDjones( 10.0f, 11.0f ) ) );

		///////////////////////////////////////////////////////////////////////////////
		// Устанавливаем распределение
		///////////////////////////////////////////////////////////////////////////////
		if( getTotalProcessors() >= 2 )
		{
			switch( getCurrentProcessor() )
			{
			case 0:
				distribution.particles.assigned.push_back( 1 );
				distribution.particles.related[1].push_back( 0 );
				distribution.particlePairs.related[1].push_back( std::make_pair( 0, 1 ) );
				break;
			case 1:
				distribution.particles.assigned.push_back( 0 );
				distribution.particles.related[0].push_back( 1 );
				distribution.particlePairs.assigned.push_back( std::make_pair( 0, 1 ) );
				break;
			}
		}
		else
		{
			distribution.particles.assigned.push_back( 0 );
			distribution.particles.assigned.push_back( 1 );
			distribution.particlePairs.assigned.push_back( std::make_pair( 0, 1 ) );
		}
	}

	SolidSystem::ParticleSystemPtr particleSystem;
	SolidSystem::calculation::ParticleDistribution distribution;
};

TEST_F( SynchronizerTest, SettingProcessors )
{
	using namespace SolidSystem::calculation;
	using namespace SolidSystem;

	///////////////////////////////////////////////////////////////////////////////
	// Устанавливаем синхронизатор
	///////////////////////////////////////////////////////////////////////////////
	ForceMatrixSynchronizer synchronizer;
	const int currentProcessor = synchronizer.getCurrentProcessor();
	const int totalProcessors = synchronizer.getProcessorsNumber();
	ASSERT_TRUE( currentProcessor >= 0 );
	ASSERT_TRUE( totalProcessors > 0 );
}

TEST_F( SynchronizerTest, ParticleSynchronization )
{
	using namespace SolidSystem::calculation;
	using namespace SolidSystem;

	////////////////////////////////////////////////////////////////
	// Меняем текущее состояние объектов
	////////////////////////////////////////////////////////////////
	ParticleSystem::State state( particleSystem );
	state.calculateForces( distribution.particlePairs.assigned );
	BOOST_FOREACH( int particleIndex, distribution.particles.assigned )
	{
		ParticleState& particleState = state.getParticleState( particleIndex );
		particleState.position[0] = 1.1f;
		particleState.velocity[0] = 2.1f;
	}

	ForceMatrixSynchronizer synchronizer;
	synchronizer.setDistribution( distribution );
	synchronizer.synchronizeParticles( state );

	for( size_t i = 0; i < particleSystem->particles().size(); i++ )
	{
		ParticleState& particleState = state.getParticleState( i );
		EXPECT_EQ( 1.1f, particleState.position[0] );
		EXPECT_EQ( 2.1f, particleState.velocity[0] );
	}
}

TEST_F( SynchronizerTest, ForceMatrixSynchronization )
{
	using namespace SolidSystem::calculation;
	using namespace SolidSystem;

	////////////////////////////////////////////////////////////////
	// Меняем текущее состояние объектов
	////////////////////////////////////////////////////////////////
	ParticleSystem::State state( particleSystem );
	state.calculateForces( distribution.particlePairs.assigned );
	BOOST_FOREACH( int particleIndex, distribution.particles.assigned )
	{
		ParticleState& particleState = state.getParticleState( particleIndex );
		particleState.position[0] += 1.0f;
		particleState.velocity[0] += 1.0f;
	}

	ForceMatrixSynchronizer synchronizer;
	synchronizer.setDistribution( distribution );
	synchronizer.synchronizeForceMatrixPairs( state );
}
