#include "gtest/gtest.h"

#include <source/Calculation/Calculator.hpp>
#include <source/Calculation/Synchronizer/ForceMatrix.hpp>
#include <source/Calculation/Decomposition/Dispensers/RowInterleaving.hpp>
#include <source/Calculation/Methods/Verlet.hpp>
#include <source/ParticleSystem/ParticleSystem.hpp>
#include <source/ParticleSystem/Particle.hpp>
#include <source/PotentialSystem/Potentials/LennardDjones.hpp>
#include <source/Animation/MDAnimation.hpp>


//TEST( CalculatorTest, Constructor )
//{
//	using namespace SolidSystem;
//	using namespace SolidSystem::calculation;
//
//	/////////////////////////////////////////////////////////////////
//	// Setting calculator
//	/////////////////////////////////////////////////////////////////
//	SynchronizerPtr synchronizer( new ForceMatrixSynchronizer() );
//	MDMethodPtr method( new VerletMethod() );
//	DispenserPtr dispenser( new RowInterleavingDispenser( 0, 1 ) );
//	Calculator calculator( dispenser, synchronizer, method );
//	calculator.init( ParticleSystemPtr(), MDParams() );
//
//	/////////////////////////////////////////////////////////////////
//	// Running calculator
//	/////////////////////////////////////////////////////////////////
//	MDAnimationPtr animation = calculator.run();
//	EXPECT_FALSE( animation );
//}

jkl 9//TEST( CalculatorTest, CommonBehavior )
//{
//	using namespace SolidSystem;
//	using namespace SolidSystem::calculation;
//
//	/////////////////////////////////////////////////////////////////
//	// Setting parameters
//	/////////////////////////////////////////////////////////////////
//	MDParams params;
//	params.startTime = 0.0f;
//	params.endTime = 1.0f;
//	params.timeDelta = 0.01f;
//
//	/////////////////////////////////////////////////////////////////
//	// Setting particles
//	/////////////////////////////////////////////////////////////////
//	math::Vector newPos1( math::ZERO_VECTOR );
//	ParticlePtr particle1( new Particle( newPos1, 1.0f ) );
//	math::Vector newPos2( math::ZERO_VECTOR );
//	newPos2[2] = 1;
//	ParticlePtr particle2( new Particle( newPos2, 1.0f ) );
//
//	/////////////////////////////////////////////////////////////////
//	// Setting particle system
//	/////////////////////////////////////////////////////////////////
//	ParticleSystemPtr system( new ParticleSystem() );
//	system->addParticle( particle1 );
//	system->addParticle( particle2 );
//	system->setPotential( particle1, particle2, MDPairPotentialPtr( new LennardDjones( 10.0f, 0.75f ) ) );
//
//	/////////////////////////////////////////////////////////////////
//	// Setting calculator
//	/////////////////////////////////////////////////////////////////
//	SynchronizerPtr synchronizer( new ForceMatrixSynchronizer() );
//	MDMethodPtr method( new VerletMethod() );
//	DispenserPtr dispenser( new RowInterleavingDispenser( 0, 1 ) );
//	Calculator calculator( dispenser, synchronizer, method );
//	calculator.init( system, params );
//
//	/////////////////////////////////////////////////////////////////
//	// Running calculator
//	/////////////////////////////////////////////////////////////////
//	MDAnimationPtr animation = calculator.run();
//	EXPECT_TRUE( animation );
//}