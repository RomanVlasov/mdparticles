#include "TestSolidSystem.hpp"
#include <source/ParticleSystem/Particle.hpp>
#include <source/PotentialSystem/MDPotential.hpp>
#include <iostream>

namespace SolidSystem {

namespace tests
{
	void runParticleSimpleTest()
	{
		std::cout << __FUNCTION__ << std::endl;

		Particle particle1;
		std::cout << particle1;

		//using namespace boost::numeric::ublas;
		math::Vector newPos( 3 );
		newPos[0] = 23;
		newPos[1] = 11;
		newPos[2] = 2;

		Particle particle2( newPos, 2.0f );
		std::cout << particle2;
	}
}

}

#include <source/ParticleSystem/ParticleSystem.hpp>

namespace SolidSystem {

namespace tests
{
	void runParticleSystemTest()
	{
		std::cout << __FUNCTION__ << std::endl;
		
		ParticleSystem system;
		system.addParticle( ParticlePtr( new Particle() ) );

		ParticlePtr particle1( new Particle() );
		ParticlePtr particle2( new Particle() );
		system.setPotential( particle1, particle2, MDPairPotentialPtr() );

		system.addParticle( particle1 );
		system.addParticle( particle2 );

		system.setPotential( particle1, particle2, MDPairPotentialPtr() );

		std::cout << "Adding particle - ok" << std::endl;
	}
}

}

#include <source/ParticleSystem/ParticleSystem.hpp>

namespace SolidSystem {

namespace tests
{
	void runSimulationTest()
	{
		std::cout << __FUNCTION__ << std::endl;
		
		ParticleSystem system;
		system.addParticle( ParticlePtr( new Particle() ) );

		math::Vector newPos1( 3 );
		ParticlePtr particle1( new Particle( newPos1, 1.0f ) );
		system.addParticle( particle1 );

		math::Vector newPos2( 3 );
		newPos2[2] = 1;
		ParticlePtr particle2( new Particle( newPos2, 1.0f ) );
		system.addParticle( particle2 );

		system.setPotential( particle1, particle2, MDPairPotentialPtr() );

		std::cout << "Simulating - ok" << std::endl;
	}
}

}