#include "MDMethodTest.hpp"

#include <iostream>
#include <fstream>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/nvp.hpp>

#include <source/Calculation/Methods/ZeroMethod.hpp>
#include <source/Calculation/Methods/Verlet.hpp>
#include <source/PotentialSystem/Potentials/LennardDjones.hpp>
#include <source/Animation/MDAnimation.hpp>

namespace SolidSystem
{
	namespace tests
	{
		MDMethodTest::MDMethodTest()
		{
		}

		MDMethodTest::~MDMethodTest()
		{
		}

		void MDMethodTest::testZeroMethod()
		{
			ParticleSystemPtr system( new ParticleSystem() );

			math::Vector newPos1( 3 );
			newPos1 = math::ZERO_VECTOR;
			ParticlePtr particle1( new Particle( newPos1, 1.0f ) );
			system->addParticle( particle1 );

			math::Vector newPos2( 3 );
			newPos2 = math::ZERO_VECTOR;
			newPos2[2] = 1;
			ParticlePtr particle2( new Particle( newPos2, 1.0f ) );
			system->addParticle( particle2 );

			system->setPotential( particle1, particle2, MDPairPotentialPtr( new LennardDjones( 10.0f, 0.5f ) ) );

			MDParams params;
			params.startTime = 0.0f;
			params.endTime = 1.0f;
			params.timeDelta = 0.2f;

			ZeroMethod zeroMethod;
			zeroMethod.init( system, params );
			zeroMethod.run();
		}

		void MDMethodTest::testVerletMethod()
		{
			ParticleSystemPtr system( new ParticleSystem() );

			math::Vector newPos1( math::ZERO_VECTOR );
			ParticlePtr particle1( new Particle( newPos1, 1.0f ) );
			system->addParticle( particle1 );

			math::Vector newPos2( math::ZERO_VECTOR );
			newPos2[2] = 1;
			ParticlePtr particle2( new Particle( newPos2, 1.0f ) );
			system->addParticle( particle2 );
			system->setPotential( particle1, particle2, MDPairPotentialPtr( new LennardDjones( 10.0f, 0.75f ) ) );

			MDParams params;
			params.startTime = 0.0f;
			params.endTime = 1.0f;
			params.timeDelta = 0.01f;

			VerletMethod method;
			method.init( system, params );
			MDAnimationPtr animation = method.run();

			if( animation )
			{
				testSaveData( *animation );
			}
			else
			{
				std::cout << "[Warning] Method result is invalid" << std::endl;
			}
		}

		void MDMethodTest::testSaveData( const MDAnimation& animation )
		{
			std::cout << "Saving animation data" << std::endl;

			std::ofstream saveFile;
			saveFile.open( "test_animation.xml" );
			boost::archive::xml_oarchive xml( saveFile );

			xml.template register_type<MDPairPotential>();
			xml.template register_type<LennardDjones>();
			xml << boost::serialization::make_nvp( "test_root", animation );
			saveFile.close();
		}

		void MDMethodTest::doRun()
		{
			std::cout << "Testing zero method" << std::endl;
			testZeroMethod();

			std::cout << "Testing Verlet method" << std::endl;
			testVerletMethod();
		}
	}
}